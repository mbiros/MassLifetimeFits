#!/bin/sh
cd /afs/cern.ch/work/m/mbiros/private/MassLifetimeFits/run
. ../start.sh
date > timer_createDataset.log
root -l -b -q ../source/BdCreateDataset.cxx\(\"$1\"\)
date >> timer_createDataset.log
cd ..
