#!/bin/sh
cd /afs/cern.ch/work/m/mbiros/private/MassLifetimeFits/run
. ../start.sh
tag=$3
date > timer_t_$1_$2_${tag}.log
root -l -b -q ../source/BdMassLifetime.cxx\(\"$1\",\"$2\",\"$3\"\)  >> timer_t_$1_$2_${tag}.log
date >> timer_t_$1_$2_${tag}.log
cd ..
