#!/bin/sh
rm plots.tex pulls.tex  cor.tex results.tex results2.tex
# rm MCTests.aux  MCTests.log  MCTests.out  MCTests.pdf   MCTests.toc

# echo \\chapter{Overall Results} >> plots.tex
IFS=
    
# echo \\chapter{Plots} >> plots.tex
for i in  *BdMassFullRun2_*.txt ; do
    frac=`sed -n -e '/mass_sigFrac/p' $i | awk '{ print $3 }' `
    chi=`head -n 1 $i | awk '{ print $2 }'`
    pullsOff3=`sed -n '2p' $i | awk '{ print $2 }'`
    pullsMin=`sed -n '3p' $i | awk '{ print $2 }' | sed 's/,//g'`
    pullsMax=`sed -n '3p' $i | awk '{ print $4 }'`
    pullsMean=`sed -n '4p' $i | awk '{ print $2 }'| sed 's/,//g'`
    pullsRMS=`sed -n '4p' $i | awk '{ print $4 }'`
    FCN=`sed -n '8p' $i |awk '{print $5}'`
    distToMin=`sed -n '8p' $i |awk '{print $(NF)}'`
    status=`sed -n '10p' $i ` # | awk '{ print $4 }'`
    failed=""
    Bmass=`sed -n -e '/fittedBmass/p' $i | awk '{ print $2 }' `
    
    chi=`printf "%.2f" $chi`
    pullsMin=`printf "%.2f" $pullsMin`
    pullsMax=`printf "%.2f" $pullsMax`
    pullsMean=`printf "%.3f" $pullsMean`
    pullsRMS=`printf "%.2f" $pullsRMS`
    frac=`printf "%.4f" $frac`
    distToMin=`printf "%.3f" $distToMin`    
    if [[ $status == *"MINIMIZE=0 HESSE=0 HESSE=0"* ]]; then
        failed=\\textcolor{green}{OK}
    else
        failed=\\textcolor{red}{F}
    fi
    
    i=`echo $i | sed 's/.txt//'`
    j=`echo $i| sed 's/BdMassFullRun2_//'`
#     j=`echo $j | sed 's/__/-/g'`
    j=`echo $j | sed 's/_/ /g'`
#     j=`echo $j | sed 's/err//g'| sed 's/mass//g'`
#     er=`echo $j| awk '{print $1 }'`
#     r=`echo $j| awk '{ print $2 }'`
    f=`echo $j| awk '{ print $1 }'`
    b=`echo $j| awk '{ print $2 }'`
    label=${f}_${b}
    echo $label $chi 
    echo  \\hyperref[fig:${label}]{$j} \& $chi \& $pullsOff3 \& $pullsMin \& $pullsMax \& $pullsMean \& $pullsRMS \& $failed \\\\ >> help.tex
    echo  $chi \\hyperref[fig:${label}]{$j} \&   $frac \&   $Bmass \&   $distToMin \\\\ >> help2.tex
#     echo  >>results.tex
    

    echo \\section{$j } >>plots.tex
    echo  \\begin{figure}[h]                                                                               >>plots.tex
    echo  \\begin{center}                                                                                  >>plots.tex
    echo  \\includegraphics[width=180mm]{${i}.pdf}                >>plots.tex
    echo  \\label{fig:${label}}                                                                           >>plots.tex
    #     echo  \\includegraphics[width=70mm]{${i}_log.pdf}             >>plots.tex
#     echo                                                                                                  >>plots.tex
    echo  \\includegraphics[width=180mm]{${i}_pull.pdf}               >>plots.tex
#     echo  \\includegraphics[width=170mm]{${i}_res.pdf}             >>plots.tex
#     echo                                                                                                  >>plots.tex
#      echo  \\includegraphics[width=100mm]{${i}_cor.pdf}             >>plots.tex

#     echo                                                                                                  >>plots.tex
#     echo  \\caption{$f. }                                                 >>plots.tex
#     echo  \\label{fig:${label}}                                                                           >>plots.tex
    echo  \\end{center}                                                                                    >>plots.tex
    echo  \\end{figure}                                                                                    >>plots.tex
    echo \\begin{table}[ht]                                                                             >> plots.tex
    echo \\begin{tabular}{\| c \| c\| c\| c\|c\|c\|}                                                    >> plots.tex
    echo \\hline                                                                                        >> plots.tex
    echo \$\\chi^2/ndf\$ \&pullsOff\$\\pm\$3 \& pullsMin \& pullsMax \& pullsMean \& pullsRMS \\\\      >> plots.tex
    echo \\hline                                                                                        >> plots.tex
    echo  \\ $chi \& $pullsOff3 \& $pullsMin \& $pullsMax \& $pullsMean \& $pullsRMS   \\\\             >> plots.tex
    echo \\hline                                                                                        >> plots.tex
    echo \\end{tabular}                                                                                 >> plots.tex
    echo \\end{table}                                                                                   >> plots.tex
    echo   \\hyperref[fig:${label}_cor]{Correlations and Residuals}                                     >>plots.tex
    echo            >>plots.tex
    echo ${status}, est. distance to min: $distToMin >>plots.tex
    echo "FCN value: "  $FCN  >>plots.tex
    echo \\newpage >>plots.tex
    echo                                                                                                >> plots.tex
    echo                                                                                                >> plots.tex     



    
    
    echo  \\vspace{-15pt}                                                                                  >>pulls.tex
    echo  \\begin{figure}[ht]                                                                              >>pulls.tex
    echo  \\begin{center}                                                                                  >>pulls.tex
#     echo  \\vspace{-25pt}                                                                                >>pulls.tex
    echo  \\includegraphics[width=180mm]{${i}_pull.pdf}                                                    >>pulls.tex
    echo  \\caption{$j  }                                                                                   >>pulls.tex
#     echo  \\label{fig:${label}}                                                                          >>pulls.tex
#     echo  \\vspace{-25pt}                                                                                >>pulls.tex
    echo  \\end{center}                                                                                    >>pulls.tex
    echo  \\end{figure}                                                                                    >>pulls.tex
    echo                                                                                                   >>pulls.tex        

    echo \\section{$j Cor } >>cor.tex
    echo  \\begin{figure}[h]                                                                               >>cor.tex
    echo  \\begin{center}                                                                                  >>cor.tex
    echo  \\includegraphics[width=170mm]{${i}_cor.pdf}                                                     >>cor.tex
    echo  \\label{fig:${label}_cor}                                                                        >>cor.tex
    echo  \\caption{$j Cor}                                                                                >>cor.tex
    echo  \\includegraphics[width=180mm]{${i}_res.pdf}                                                     >>cor.tex
    
    echo  \\end{center}                                                                                    >>cor.tex
    echo  \\end{figure}                                                                                    >>cor.tex
    echo   \\hyperref[fig:${label}]{Plot and Pulls}                                                        >>cor.tex
    echo \\newpage >>cor.tex

    done


echo \\begin{table}[ht]                     >> results.tex
echo \\begin{tabular}{ \| l  \| c \| c\| c\| c\|c\|c\|c\|}      >> results.tex
echo \\hline                                >> results.tex
echo  \&  \& \\multicolumn{5}{c\|}{Pull}\& Fit  \\\\       >> results.tex
echo  \\hline                                  >> results.tex
echo Sg and bg description \& \$\\chi^2/ndf\$ \&Off\$\\pm\$3 \& Min \& Max \& Mean \& RMS \& stat \\\\       >> results.tex
echo \\hline                                >> results.tex
echo `sort -nk 4 help.tex` >> results.tex
echo \\hline                               >> results.tex
echo \\end{tabular}                        >> results.tex
# echo \\caption{.}                          >> results.tex
echo \\label{tab:chis}                     >> results.tex
echo \\end{table}                          >> results.tex


echo \\begin{table}[ht]                     >> results2.tex
echo \\begin{tabular}{ \| l  \| c \| c\| c\|}      >> results2.tex
echo \\hline                                >> results2.tex
#echo  \&  \& \\multicolumn{5}{c\|}{Pull}\& Fit \& \\\\       >> results2.tex
# echo  \\hline                                  >> results2.tex
echo Sg and bg description  \& frac \& Bmass \& distToMin \\\\       >> results2.tex
echo \\hline                                >> results2.tex
echo `sort -nk 1 help2.tex` > help3.tex
echo `awk '{$1=""; print $0}' help3.tex ` >> results2.tex
echo \\hline                               >> results2.tex
echo \\end{tabular}                        >> results2.tex
# echo \\caption{.}                          >> results.tex
echo \\label{tab:chis}                     >> results2.tex
echo \\end{table}                          >> results2.tex


rm help.tex help2.tex help3.tex
export PATH=/cvmfs/sft.cern.ch/lcg/external/texlive/2016/bin/x86_64-linux:$PATH
pdflatex MassFit.tex 
#TODO if flag
pdflatex MassFit.tex > /dev/null
pdflatex MassFit.tex > /dev/null 
rm MassFit.aux MassFit.log MassFit.out MassFit.toc
