<h2>This repository is used to produce Mass and Lifetime fits for Bd and B+ decays.</h2>

The source files are stored in /source

The input files are stored on eos, but you can produce RooDatasets that will be stored in /data to speed up the code
(during the first run set flag readRooData=false or copy from "/eos/user/r/ranovotn/public/BdNtuples/RooDatasetBd.root").
The RooDataset contain lifetime correction as weight!
The output images are stored in /run directory.

=======================================================================

First run
> cp /eos/user/r/ranovotn/public/BdNtuples/RooDatasetBd.root ./data 

lxplus run
>. ./start.sh

>cd run

>root -l -b -q ../source/BdMass.cxx\\(\\"john\\",\\"tagIfYouWantToDistinguishOutputs\\",false\\)
\# true if you use per-candidate error

batch run
> mkdir logfiles/ 

>mkdir logfiles/batchDetail/

>condor_submit submitJobs.sub
\# need to change path to runner.sh in submitJobs.sub
\# takes arguments from batchArgsList


Plot results
>cd run

>. ./generatePdf.sh
