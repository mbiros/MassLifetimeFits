 #include "RooRealVar.h"
 #include "RooDataSet.h"
 #include "RooDataHist.h"
 #include "RooGaussian.h"
 #include "RooGaussModel.h"
 #include "RooExponential.h"
 #include "TCanvas.h"
 #include "RooPlot.h"
 #include "TTree.h"
 #include "TFile.h"
 #include "TRandom.h"
 #include "RooAddPdf.h"
 #include "RooDecay.h"
 #include "RooProdPdf.h"
 #include "TH1F.h"
 #include "TH2F.h"
 #include "RooHistPdf.h"
 #include "RooFormulaVar.h"
 #include "RooProduct.h"
 #include "TRandom3.h"
 #include "RooHist.h"
 #include "RooFitResult.h"
 #include "RooCategory.h"
 #include "RooWorkspace.h"

using namespace RooFit;

    Double_t massPDG = 5279.65;

    Double_t massMin = 5000;
    Double_t massMax = 5650;

    Double_t tauMin = -2;
    Double_t tauMax = 20;

    Double_t tauErrMin = .018;
    Double_t tauErrMax = .12;

    Double_t massErrMin = 10;
    Double_t massErrMax = 100;

    Double_t ptMin = 10;
    Double_t ptMax = 100;

    set<TString> years {"2015","2016A", "2016B", "2016C", "2017", "2018"};

RooDataSet * readPeriod(TString year, bool useTriggers, RooCategory* yearsCategory){
   TFile *file = new TFile("/eos/user/r/ranovotn/public/BdNtuples/BdKstarFullRun2.root","READ");
   //TFile *file = new TFile("/data0/novotnyr/tmp/BdKstarSlimming/BdKstarFullRun2.root","READ");

    TTree* tree = (TTree*)file->Get("BdBestChi");
    TTree* treeTrig = (TTree*)file->Get("BdTriggers");
    tree->AddFriend(treeTrig);

    RooRealVar *mass = new RooRealVar("B_mass", "B_mass", massMin, massMax);
    RooRealVar *time = new RooRealVar("B_tau_MinA0", "B_tau_MinA0", tauMin, tauMax);
    RooRealVar *massErr = new RooRealVar("B_mass_err", "B_mass_err", massErrMin, massErrMax);
    RooRealVar *timeErr = new RooRealVar("B_tau_MinA0_err", "B_tau_MinA0_err", tauErrMin, tauErrMax);
    RooRealVar *pt = new RooRealVar("B_pT", "B_pT", ptMin, ptMax);

        TString newCuts = "( ( ( abs(Jpsi_mu1_eta)<=1.05 && abs(Jpsi_mu2_eta)<=1.05 ) && ( Jpsi_mass>2959 && Jpsi_mass<3229 ) ) || ( ( abs(Jpsi_mu1_eta)>1.05 && abs(Jpsi_mu1_eta)<=2.5 && abs(Jpsi_mu2_eta)>1.05 && abs(Jpsi_mu2_eta)<=2.5 ) && ( Jpsi_mass>2852 && Jpsi_mass<3332 ) ) || ( ( ( abs(Jpsi_mu1_eta)<=1.05 && abs(Jpsi_mu2_eta)>1.05 && abs(Jpsi_mu2_eta)<=2.5 ) || ( abs(Jpsi_mu1_eta)>1.05 && abs(Jpsi_mu1_eta)<=2.5 && abs(Jpsi_mu2_eta)<=1.05 ) ) && ( Jpsi_mass>2913 && Jpsi_mass<3273 ) ) )";
    newCuts += Form("&& (B_tau_MinA0>%f && B_tau_MinA0<%f)",tauMin,tauMax);
    newCuts += Form("&& (B_tau_MinA0_err>%f && B_tau_MinA0_err<%f)",tauErrMin,tauErrMax);
    newCuts += Form("&& (B_mass>%f && B_mass<%f)",massMin,massMax);
    newCuts += Form("&& (B_mass_err>%f && B_mass_err<%f)",massErrMin,massErrMax);
    newCuts += "&& (B_trk1_pT>3000 && B_trk2_pT>1000)";
    newCuts += "&& (B_mu1_pT>4000 && B_mu2_pT>4000)";
    newCuts += "&& pass_GRL";
    newCuts += "&& B_pT>10000 ";

    if(year == "2015"){
        newCuts += "&& (run_number<284700)";
        yearsCategory->setLabel("2015");
    }  else if(year == "2016A"){
        newCuts += "&& (run_number<302737 && run_number >= 296400)";
        yearsCategory->setLabel("2016A");
    } else if(year == "2016B"){
        newCuts += " && (run_number<302737 && run_number >= 296400)";
        yearsCategory->setLabel("2016B");
    } else if(year == "2016C"){
         newCuts += " && (run_number>=302737 && run_number<=311481)";
         yearsCategory->setLabel("2016C");
    } else if(year == "2017"){
            newCuts += "&& (run_number>=324320 && run_number < 341649)";
            yearsCategory->setLabel("2017");
    }  else if(year == "2018"){
            newCuts += "&& (run_number>=348197)";
            yearsCategory->setLabel("2018");
    }
    if(useTriggers){
        if(year == "2015"){
            newCuts += "&&  (HLT_2mu4_bJpsimumu_noL2 == 1 || HLT_mu6_mu4_bBmumuxv2 == 1 || HLT_mu18_2mu0noL1_JpsimumuFS == 1)";
        } else if(year == "2016A"){
             newCuts += "&& ((HLT_2mu6_bBmumuxv2 == 1) || (HLT_mu6_mu4_bBmumuxv2 == 1) || (HLT_mu10_mu6_bBmumuxv2 == 1))";
        } else if(year == "2016B"){
             newCuts += "&& ((HLT_2mu6_bBmumuxv2 == 0) && (HLT_mu6_mu4_bBmumuxv2 == 0) && (HLT_mu10_mu6_bBmumuxv2 == 0)) && (HLT_mu20_2mu0noL1_JpsimumuFS == 1 || HLT_mu10_mu6_bJpsimumu == 1 || HLT_mu6_mu4_bJpsimumu == 1)";
        } else if(year == "2016C"){
             newCuts += " &&  (HLT_2mu6_bJpsimumu_delayed == 1 || HLT_2mu6_bBmumuxv2_delayed == 1 || HLT_mu6_mu4_bBmumuxv2_delayed==1 || HLT_2mu4_bJpsimumu_L1BPH_2M8_2MU4 == 1 || HLT_mu6_mu4_bJpsimumu_delayed == 1 || HLT_2mu6_bBmumuxv2 == 1 || HLT_mu6_mu4_bJpsimumu == 1 || HLT_mu20_2mu4_JpsimumuL2 == 1)";
        } else if(year == "2017"){
          //newCuts += "&&  (HLT_mu6_mu4_bBmumux_BsmumuPhi_L1BPH_2M9_MU6MU4_BPH_0DR15_MU6MU4 == 1 || HLT_mu11_mu6_bDimu == 1 || HLT_2mu4_bBmumux_BsmumuPhi_L1BPH_2M9_2MU4_BPH_0DR15_2MU4 == 1 || HLT_2mu6_bJpsimumu_L1BPH_2M9_2MU6_BPH_2DR15_2MU6 == 1 || HLT_mu20_2mu2noL1_JpsimumuFS == 1 ||  HLT_2mu6_bBmumuxv2_L1LFV_MU6== 1 || HLT_3mu4_bJpsi == 1 || HLT_mu22_mu8noL1 == 1 || HLT_mu11_2mu4noL1_bNocut_L1MU11_2MU6 == 1 ||  HLT_mu20_2mu4_JpsimumuL2== 1)";
          newCuts += "&& (HLT_2mu6_bJpsimumu_L1BPH_2M9_2MU6_BPH_2DR15_2MU6 == 1 || HLT_2mu6_bBmumuxv2_L1LFV_MU6 == 1 || HLT_mu11_mu6_bDimu == 1 || HLT_mu11_mu6_bBmumuxv2 ==1 || HLT_mu20_2mu2noL1_JpsimumuFS == 1 || HLT_mu6_mu4_bBmumuxv2 == 1 || HLT_2mu14 == 1 || HLT_mu50 == 1)";
   
     }  else if(year == "2018"){
            //newCuts += "&&  (HLT_mu6_mu4_bBmumux_BsmumuPhi_L1BPH_2M9_MU6MU4_BPH_0DR15_MU6MU4 == 1 || HLT_mu11_mu6_bDimu == 1 || HLT_2mu4_bBmumux_BsmumuPhi_L1BPH_2M9_2MU4_BPH_0DR15_2MU4 == 1 || HLT_2mu6_bJpsimumu_L1BPH_2M9_2MU6_BPH_2DR15_2MU6 == 1 || HLT_mu20_2mu2noL1_JpsimumuFS == 1 ||  HLT_2mu6_bBmumuxv2_L1LFV_MU6== 1 || HLT_3mu4_bJpsi == 1 || HLT_mu22_mu8noL1 == 1 || HLT_mu11_2mu4noL1_bNocut_L1MU11_2MU6 == 1 ||  HLT_mu20_2mu4_JpsimumuL2== 1)";
           newCuts += "&& (HLT_2mu6_bJpsimumu_L1BPH_2M9_2MU6_BPH_2DR15_2MU6 == 1 || HLT_2mu6_bBmumuxv2_L1LFV_MU6 == 1 || HLT_mu11_mu6_bDimu == 1 || HLT_mu11_mu6_bBmumuxv2 == 1 || HLT_mu20_2mu2noL1_JpsimumuFS == 1 || HLT_mu6_mu4_bBmumuxv2 == 1 || HLT_2mu14 == 1 || HLT_mu50 == 1 || HLT_mu22_mu8noL1 == 1 || HLT_mu20_2mu4_JpsimumuL2 == 1)";
        }
    }
    cout << "Cuts used: "<< newCuts <<endl;

    Long64_t nEvents = tree->Draw("1", newCuts, "goff");
    tree->SetEstimate(nEvents);
    tree->Draw("B_mass:B_tau_MinA0:B_mass_err:B_tau_MinA0_err:B_pT/1000", newCuts, "para goff");

    RooAbsData::setDefaultStorageType(RooAbsData::Tree);
    RooArgSet*  vars = new RooArgSet(*mass, *time, *massErr, *timeErr, *pt,*yearsCategory);
    RooDataSet* data = new RooDataSet("data", newCuts, *vars);
    TIterator *vars_it = vars->createIterator();
    for ( Long64_t i = 0; i < nEvents; i++ ) {
        vars_it->Reset();
        Int_t j = 0;
        for ( auto var = (RooRealVar*)vars_it->Next(); var != NULL ; var = (RooRealVar*)vars_it->Next() ) {
            if(j!=vars->getSize()-1)var->setVal(tree->GetVal(j)[i]);
            j++;
        }
        data->add(*vars);
    }
//add weights
        TString tauWeight = "1.0";
        /*if(year == "2015"){ tauWeight = "(1./(0.250528*(1-6.078965*(TMath::Erf((@-143.186803)/79.912155)+1))))/4.285338";
        } else if(year == "2016A"){tauWeight = "(1./(0.200441*(1-0.412051*(TMath::Erf((@-10.989815)/9.750277)+1))))/5.227969";
        } else if(year == "2016B"){tauWeight = "(1./(0.041851*(1-0.423196*(TMath::Erf((@--13.842645)/3.238083)+1))))/155.552211";
        } else if(year == "2016C"){tauWeight = "(1./(0.237263*(1-3.565163*(TMath::Erf((@-137.356915)/88.127249)+1))))/4.673041";
        } else if(year == "2017"){tauWeight =  "(1./(0.403429*(1-0.388560*(TMath::Erf((@--34.960536)/39.639180)+1))))/8.117291";
        }  else if(year == "2018"){tauWeight = "(1./(0.399101*(1-0.571840*(TMath::Erf((@--32.200061)/168.608124)+1))))/8.177265";
        }*/
        /*if(year == "2015"){ tauWeight = "(1./(0.106815*(1-0.460711*(TMath::Erf((@-26.397812)/10.044872)+1))))/9.362877";
        } else if(year == "2016A"){tauWeight = "(1./(0.087153*(1-0.435989*(TMath::Erf((@-12.566945)/8.083334)+1))))/11.615359";
        } else if(year == "2016B"){tauWeight = "(1./(0.028333*(1-0.448055*(TMath::Erf((@--31.171420)/3.533474)+1))))/339.726065";
        } else if(year == "2016C"){tauWeight = "(1./(0.097839*(1-0.463864*(TMath::Erf((@-26.396249)/10.297728)+1))))/10.222212";
        } else if(year == "2017"){tauWeight =  "(1./(0.054760*(1-0.427565*(TMath::Erf((@-23.354320)/10.572250)+1))))/18.275321";
        }  else if(year == "2018"){tauWeight = "(1./(0.054609*(1-0.471264*(TMath::Erf((@-24.096971)/10.823185)+1))))/18.326182";
        }*/
	if(year == "2015"){ tauWeight = "(1./(0.111627*(1-0.604399*(TMath::Erf((@0-28.902776)/14.511842)+1))))/8.984731";
        } else if(year == "2016A"){tauWeight = "(1./(0.092605*(1-0.393762*(TMath::Erf((@0-9.786387)/7.386000)+1))))/11.064105";
        } else if(year == "2016B"){tauWeight = "(1./(0.028958*(1-0.447239*(TMath::Erf((@0--35.324645)/8.216771)+1))))/327.258261";
        } else if(year == "2016C"){tauWeight = "(1./(0.102441*(1-0.639164*(TMath::Erf((@0-29.658980)/15.557181)+1))))/9.805719";
        } else if(year == "2017"){tauWeight =  "(1./(0.097549*(1-0.800386*(TMath::Erf((@0-32.691955)/19.259848)+1))))/10.387425";
        }  else if(year == "2018"){tauWeight = "(1./(0.096281*(1-0.499653*(TMath::Erf((@0-25.367223)/13.913118)+1))))/10.438052";
        }
        RooFormulaVar wFunc("w","event tau-weight",tauWeight,*time) ;
        RooRealVar* w = (RooRealVar*) data->addColumn(wFunc) ;

    cout << year<<": Total number of events:" <<data->numEntries() <<endl;
    data->Print();
    return data;
}

void readData(bool useTriggers, RooCategory *yearsCategory, TString name){
    
    
    RooAbsData::setDefaultStorageType(RooAbsData::Tree);
    RooDataSet* wdata = NULL;
    RooDataSet* wdataTmp[6];
    cout << "Use trigger selection: "<< useTriggers <<endl;
    cout << "Loading 2015..."<<endl;
    wdataTmp[0]=readPeriod("2015",useTriggers,yearsCategory);
    cout << "Loading 2016A..."<<endl;
    wdataTmp[1]=readPeriod("2016A",useTriggers,yearsCategory);
    cout << "Loading 2016B..."<<endl;
    wdataTmp[2]=readPeriod("2016B",useTriggers,yearsCategory);
    cout << "Loading 2016C..."<<endl;
    wdataTmp[3]=readPeriod("2016C",useTriggers,yearsCategory);
    cout << "Loading 2017..."<<endl;
    wdataTmp[4]=readPeriod("2017",useTriggers,yearsCategory);
    cout << "Loading 2018..."<<endl;
    wdataTmp[5]=readPeriod("2018",useTriggers,yearsCategory);

    wdata = wdataTmp[0];
    wdata->append(*wdataTmp[1]);
    wdata->append(*wdataTmp[2]);
    wdata->append(*wdataTmp[3]);
    wdata->append(*wdataTmp[4]);
    wdata->append(*wdataTmp[5]);
    //wdata =readPeriod("2018",useTriggers,yearsCategory);
    ///save dataset to file
    TFile* f = new TFile(name+".root","RECREATE") ;
    wdata->Write();
    f->Close();

//    return wdata;
}

void BdCreateDataset(TString tag,TString path ="../data/", TString name ="RooDatasetBd_"){
    //name = "../data/RooDatasetBd"
    bool useTriggers = true;
    
    RooCategory* yearsCategory = new RooCategory("yearsCategory","yearsCategory");
    yearsCategory->defineType("2015" ) ;
    yearsCategory->defineType("2016A") ;
    yearsCategory->defineType("2016B") ;
    yearsCategory->defineType("2016C") ;
    yearsCategory->defineType("2017" ) ;
    yearsCategory->defineType("2018" ) ;
    
    readData(useTriggers,yearsCategory,path+name+tag);
}
