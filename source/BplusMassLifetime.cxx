 #include "RooRealVar.h"
 #include "RooDataSet.h"
 #include "RooDataHist.h"
 #include "RooGaussian.h"
 #include "RooGaussModel.h"
 #include "RooExponential.h"
 #include "TCanvas.h"
 #include "RooPlot.h"
 #include "TTree.h"
 #include "TFile.h"
 #include "TH1D.h"
 #include "TRandom.h"
 #include "RooAddPdf.h"
 #include "RooDecay.h"
 #include "RooProdPdf.h"
 #include "TH1F.h"
 #include "TH2F.h"
 #include "RooHistPdf.h"
 #include "RooFormulaVar.h"
 #include "RooProduct.h"
 #include "RooCategory.h"
 #include "RooPDFs/RooTanhPdf.h"
 #include "RooPDFs/RooTanhPdf.cxx"
 #include "TRandom3.h"
 #include "RooHist.h"
 #include "RooFitResult.h"

 #include "RooPDFs/RooDSCB.h"
 #include "RooPDFs/RooDSCB.cxx"

using namespace RooFit;

    Double_t massMin = 5000;
    Double_t massFitMin = 5050;
    Double_t massMax = 5600;

    Double_t tauMin = -2;
    Double_t tauMax = 30;

    Double_t tauErrMin = .018;
    Double_t tauErrMax = .165;

    Double_t massErrMin = 0;
    Double_t massErrMax = 100;

    Double_t ptMin = 0;
    Double_t ptMax = 100;

    set<TString> years {"2015","2016A", "2016B", "2016C", "2017", "2018"};

RooDataSet * readPeriod(TString year, bool useTriggers, RooCategory* yearsCategory){
    TFile *file = new TFile("/eos/user/r/ranovotn/public/BplusNtuplesNew/BplusFullRun2.root","READ");

    TTree* tree = (TTree*)file->Get("BplusBestChi");
    TTree* treeTrig = (TTree*)file->Get("BplusTriggers");
    tree->AddFriend(treeTrig);

    RooRealVar *mass = new RooRealVar("B_mass", "B_mass", massMin, massMax);
    RooRealVar *time = new RooRealVar("B_tau_MinA0", "B_tau_MinA0", tauMin, tauMax);
    RooRealVar *massErr = new RooRealVar("B_mass_err", "B_mass_err", massErrMin, massErrMax);
    RooRealVar *timeErr = new RooRealVar("B_tau_MinA0_err", "B_tau_MinA0_err", tauErrMin, tauErrMax);
    RooRealVar *pt = new RooRealVar("B_pT", "B_pT", ptMin, ptMax);

    TString newCuts = "";
    newCuts += "( ( ( abs(Jpsi_mu1_eta)<=1.05 && abs(Jpsi_mu2_eta)<=1.05 ) && ( Jpsi_mass>2959 && Jpsi_mass<3229 ) ) || ( ( abs(Jpsi_mu1_eta)>1.05 && abs(Jpsi_mu1_eta)<=2.5 && abs(Jpsi_mu2_eta)>1.05 && abs(Jpsi_mu2_eta)<=2.5 ) && ( Jpsi_mass>2852 && Jpsi_mass<3332 ) ) || ( ( ( abs(Jpsi_mu1_eta)<=1.05 && abs(Jpsi_mu2_eta)>1.05 && abs(Jpsi_mu2_eta)<=2.5 ) || ( abs(Jpsi_mu1_eta)>1.05 && abs(Jpsi_mu1_eta)<=2.5 && abs(Jpsi_mu2_eta)<=1.05 ) ) && ( Jpsi_mass>2913 && Jpsi_mass<3273 ) ) )";
    newCuts += Form("&& (B_tau_MinA0>%f && B_tau_MinA0<%f)",tauMin,tauMax);
    newCuts += Form("&& (B_tau_MinA0_err>%f && B_tau_MinA0_err<%f)",tauErrMin,tauErrMax);
    newCuts += Form("&& (B_mass>%f && B_mass<%f)",massMin,massMax);
    newCuts += Form("&& (B_mass_err>%f && B_mass_err<%f)",massErrMin,massErrMax);
    newCuts += "&& B_trk1_pT>3000";
    newCuts += "&& (B_mu1_pT>6000 && B_mu2_pT>6000)";
    newCuts += "&& pass_GRL";
    newCuts += "&& B_pT>10000 ";
    newCuts += "&& B_chi2_ndof<3.0  ";

    if(year == "2015"){
        newCuts += "&& (run_number<284700)";
        yearsCategory->setLabel("2015");
    }  else if(year == "2016A"){
        newCuts += "&& (run_number<302737 && run_number >= 296400)";
        yearsCategory->setLabel("2016A");
    } else if(year == "2016B"){
        newCuts += " && (run_number<302737 && run_number >= 296400)";
        yearsCategory->setLabel("2016B");
    } else if(year == "2016C"){
         newCuts += " && (run_number>=302737 && run_number<=311481)";
         yearsCategory->setLabel("2016C");
    } else if(year == "2017"){
            newCuts += "&& (run_number>=324320 && run_number < 341649)";
            yearsCategory->setLabel("2017");
    }  else if(year == "2018"){
            newCuts += "&& (run_number>=348197)";
            yearsCategory->setLabel("2018");
    }
    if(useTriggers){
        if(year == "2015"){
            newCuts += "&&  (HLT_2mu4_bJpsimumu_noL2 == 1 || HLT_mu6_mu4_bBmumuxv2 == 1 || HLT_mu18_2mu0noL1_JpsimumuFS == 1)";
        } else if(year == "2016A"){
             newCuts += "&& ((HLT_2mu6_bBmumuxv2 == 1) || (HLT_mu6_mu4_bBmumuxv2 == 1) || (HLT_mu10_mu6_bBmumuxv2 == 1))";
        } else if(year == "2016B"){
             newCuts += "&& ((HLT_2mu6_bBmumuxv2 == 0) && (HLT_mu6_mu4_bBmumuxv2 == 0) && (HLT_mu10_mu6_bBmumuxv2 == 0)) && (HLT_mu20_2mu0noL1_JpsimumuFS == 1 || HLT_mu10_mu6_bJpsimumu == 1 || HLT_mu6_mu4_bJpsimumu == 1)";
        } else if(year == "2016C"){
             newCuts += " &&  (HLT_2mu6_bJpsimumu_delayed == 1 || HLT_2mu6_bBmumuxv2_delayed == 1 || HLT_mu6_mu4_bBmumuxv2_delayed==1 || HLT_2mu4_bJpsimumu_L1BPH_2M8_2MU4 == 1 || HLT_mu6_mu4_bJpsimumu_delayed == 1 || HLT_2mu6_bBmumuxv2 == 1 || HLT_mu6_mu4_bJpsimumu == 1 || HLT_mu20_2mu4_JpsimumuL2 == 1)";
        } else if(year == "2017"){
            //newCuts += "&& ( HLT_2mu6_bJpsimumu_L1BPH_2M9_2MU6_BPH_2DR15_2MU6 == 1 || HLT_2mu6_bBmumuxv2_L1LFV_MU6 == 1 || HLT_mu11_mu6_bDimu == 1 || HLT_mu11_mu6_bBmumuxv2 == 1 || HLT_mu20_2mu2noL1_JpsimumuFS == 1 || HLT_mu6_mu4_bBmumuxv2 == 1 || HLT_2mu14 == 1 || HLT_mu50 == 1 )";
            newCuts += "&& ( HLT_2mu6_bJpsimumu_L1BPH_2M9_2MU6_BPH_2DR15_2MU6 == 1 || HLT_2mu6_bBmumuxv2_L1LFV_MU6 == 1 || HLT_mu11_mu6_bDimu == 1 || HLT_mu11_mu6_bBmumuxv2 == 1 || HLT_mu20_2mu2noL1_JpsimumuFS == 1 || HLT_mu6_mu4_bBmumuxv2 == 1 || HLT_2mu14 == 1 || HLT_mu50 == 1 || HLT_mu6_mu4_bBmumux_BsmumuPhi_L1BPH_2M9_MU6MU4_BPH_0DR15_MU6MU4 == 1 || HLT_2mu4_bBmumux_BsmumuPhi_L1BPH_2M9_2MU4_BPH_0DR15_2MU4 == 1 || HLT_mu22_mu8noL1 == 1 || HLT_mu11_2mu4noL1_bNocut_L1MU11_2MU6 == 1 || HLT_mu20_2mu4_JpsimumuL2 == 1 )";
        }  else if(year == "2018"){
            newCuts += "&& ( HLT_2mu6_bJpsimumu_L1BPH_2M9_2MU6_BPH_2DR15_2MU6 == 1 || HLT_2mu6_bBmumuxv2_L1LFV_MU6 == 1 || HLT_mu11_mu6_bDimu == 1 || HLT_mu11_mu6_bBmumuxv2 == 1 || HLT_mu20_2mu2noL1_JpsimumuFS == 1 || HLT_mu6_mu4_bBmumuxv2 == 1 || HLT_2mu14 == 1 || HLT_mu50 == 1 || HLT_mu6_mu4_bBmumux_BsmumuPhi_L1BPH_2M9_MU6MU4_BPH_0DR15_MU6MU4 == 1 || HLT_2mu4_bBmumux_BsmumuPhi_L1BPH_2M9_2MU4_BPH_0DR15_2MU4 == 1 || HLT_mu22_mu8noL1 == 1 || HLT_mu11_2mu4noL1_bNocut_L1MU11_2MU6 == 1 || HLT_mu20_2mu4_JpsimumuL2 == 1 )";
        }
    }
    cout << "Cuts used: "<< newCuts <<endl;

    Long64_t nEvents = tree->Draw("1", newCuts, "goff");
    tree->SetEstimate(nEvents);
    tree->Draw("B_mass:B_tau_MinA0:B_mass_err:B_tau_MinA0_err:B_pT/1000", newCuts, "para goff");

    RooAbsData::setDefaultStorageType(RooAbsData::Tree);
    RooArgSet*  vars = new RooArgSet(*mass, *time, *massErr, *timeErr, *pt,*yearsCategory);
    RooDataSet* data = new RooDataSet("data", newCuts, *vars);
    TIterator *vars_it = vars->createIterator();
    for ( Long64_t i = 0; i < nEvents; i++ ) {
        vars_it->Reset();
        Int_t j = 0;
        for ( auto var = (RooRealVar*)vars_it->Next(); var != NULL ; var = (RooRealVar*)vars_it->Next() ) {
            if(j!=vars->getSize()-1)var->setVal(tree->GetVal(j)[i]);
            j++;
        }
        data->add(*vars);
    }
//add weights
        TString tauWeight = "1.0";
        if(year == "2015"){ tauWeight ="(1./(0.267910*(1-0.470074*(TMath::Erf((@-28.462532)/11.954002)+1))))/3.733928";
        } else if(year == "2016A"){tauWeight = "(1./(0.225389*(1-0.321787*(TMath::Erf((@-7.410386)/5.345489)+1))))/4.509227";
        } else if(year == "2016B"){tauWeight = "(1./(0.110067*(1-0.431888*(TMath::Erf((@--102.878824)/17.384855)+1))))/66.694587";
        } else if(year == "2016C"){tauWeight = "(1./(0.224793*(1-0.458935*(TMath::Erf((@-27.626711)/11.693549)+1))))/4.450244";
        } else if(year == "2017"){tauWeight = "(1./(0.228279*(1-0.464671*(TMath::Erf((@-27.512423)/12.290344)+1))))/4.383760";
        }  else if(year == "2018"){tauWeight = "(1./(0.228279*(1-0.464671*(TMath::Erf((@-27.512423)/12.290344)+1))))/4.383760";  
        }
        RooFormulaVar wFunc("w","event tau-weight",tauWeight,*time) ;
        RooRealVar* w = (RooRealVar*) data->addColumn(wFunc) ;

    cout << year<<": Total number of events:" <<data->numEntries() <<endl;
    return data;
}

RooDataSet * readData(bool useTriggers, RooCategory *yearsCategory){
    RooAbsData::setDefaultStorageType(RooAbsData::Tree);
    RooDataSet* wdata = NULL;
    RooDataSet* wdataTmp[6];
    cout << "Use trigger selection: "<< useTriggers <<endl;
    cout << "Loading 2015..."<<endl;
    wdataTmp[0]=readPeriod("2015",useTriggers,yearsCategory);
    cout << "Loading 2016A..."<<endl;
    wdataTmp[1]=readPeriod("2016A",useTriggers,yearsCategory);
    cout << "Loading 2016B..."<<endl;
    wdataTmp[2]=readPeriod("2016B",useTriggers,yearsCategory);
    cout << "Loading 2016C..."<<endl;
    wdataTmp[3]=readPeriod("2016C",useTriggers,yearsCategory);
    cout << "Loading 2017..."<<endl;
    wdataTmp[4]=readPeriod("2017",useTriggers,yearsCategory);
    cout << "Loading 2018..."<<endl;
    wdataTmp[5]=readPeriod("2018",useTriggers,yearsCategory);

    wdata = wdataTmp[0];
    wdata->append(*wdataTmp[1]);
    wdata->append(*wdataTmp[2]);
    wdata->append(*wdataTmp[3]);
    wdata->append(*wdataTmp[4]);
    wdata->append(*wdataTmp[5]);
    ///save dataset to file
    TFile* f = new TFile("../data/RooDatasetBplus.root","RECREATE") ;
    wdata->Write();
    f->Close();

   return wdata;
}

void BplusMassLifetime(TString year = "",bool useWeight = true, bool readRooData = true,int sample = -1){
    RooCategory* yearsCategory = new RooCategory("yearsCategory","yearsCategory");
    yearsCategory->defineType("2015" ) ;
    yearsCategory->defineType("2016A") ;
    yearsCategory->defineType("2016B") ;
    yearsCategory->defineType("2016C") ;
    yearsCategory->defineType("2017" ) ;
    yearsCategory->defineType("2018" ) ;

    bool useTriggers = true;
    bool useMassPCE = false;

    bool backgroundPunziSubtractMass =false;
    bool backgroundPunziSubtractTime =false;


    Double_t sidebandCutoff = 150;//was 180 can be 130?
    Double_t signaCutoff = 50;

    int nBins = 40;
    int nCPU = 3;
    //READ the data from file and put them to the RooDataset
    RooFitResult *result;
    RooRealVar *mass = new RooRealVar("B_mass", "B_mass", massMin, massMax);
    RooRealVar *time = new RooRealVar("B_tau_MinA0", "B_tau_MinA0", tauMin, tauMax);
    RooRealVar *massErr = new RooRealVar("B_mass_err", "B_mass_err", massErrMin, massErrMax);
    RooRealVar *timeErr = new RooRealVar("B_tau_MinA0_err", "B_tau_MinA0_err", tauErrMin, tauErrMax);
    RooRealVar *pt = new RooRealVar("B_pT", "B_pT", ptMin, ptMax);
    RooRealVar *weight = new RooRealVar("w", "w", 0, 100);
    RooArgSet*  vars = new RooArgSet(*mass, *time, *massErr, *timeErr, *pt,*yearsCategory,*weight);

    RooDataSet* wdata = NULL;    
    RooDataSet* dataTmp = NULL;    
    RooDataSet* data = NULL;    
    if(readRooData){
        TFile* f = new TFile("../data/RooDatasetBplus.root","READ") ;
        dataTmp = (RooDataSet*)f->Get("data"); 
        f->Close();
    }
    else{
    dataTmp = readData(useTriggers,yearsCategory);
    }   

    if(years.find(year) != years.end()){
        data = (RooDataSet*) dataTmp->reduce(*vars,Form("yearsCategory==yearsCategory::%s",year.Data())) ;
    }else if(year == "2016"){
        data=(RooDataSet*) dataTmp->reduce(*vars,"yearsCategory==yearsCategory::2016A || yearsCategory==yearsCategory::2016C") ;
    }
    else{
        //data=(RooDataSet*) dataTmp->reduce(*vars,"yearsCategory==yearsCategory::2015 || yearsCategory==yearsCategory::2016A|| yearsCategory==yearsCategory::2016B|| yearsCategory==yearsCategory::2016C || yearsCategory==yearsCategory::2017 || yearsCategory==yearsCategory::2018") ;
       data=(RooDataSet*) dataTmp->reduce(*vars,"yearsCategory==yearsCategory::2015 || yearsCategory==yearsCategory::2016A || yearsCategory==yearsCategory::2016C || yearsCategory==yearsCategory::2017 || yearsCategory==yearsCategory::2018") ;
    }
    data->Print();

    if(useWeight){
        wdata = new RooDataSet(data->GetName(),data->GetTitle(),data,*data->get(),0,weight->GetName()) ;
    }
    else wdata = data;

    if (sample > 0) {
    Long64_t nOrig = wdata->numEntries();
    TString title = wdata->GetTitle();
    title += " Random sample ( ";
    title += sample;
    title += " / ";
    title += nOrig;
    title += " )";
    RooArgSet*  vars = new RooArgSet(*mass, *time, *massErr, *timeErr, *pt);
    RooDataSet *newDataset = new RooDataSet("wdataRandomSample", title, *vars);
    TRandom3 rand(0);
    for ( Long64_t i = 0; i < sample; i++ ) {
        Long64_t j = rand.Uniform( 0 , nOrig );
        newDataset->add( *(wdata->get( j )) );
    }
    cout << "INFO: Created random sample (" << sample << " from " << nOrig << ")" << endl;
    wdata=newDataset;

    }
    
    RooDataSet* wdataFit = (RooDataSet*)wdata->reduce(Form("B_mass>%f",massFitMin));
    mass->setRange("fitRange",massFitMin,massMax);
    
    ///Prepare model
    ////Signal
    //Mass
    RooRealVar *mass_mean      = new RooRealVar ("massSignal_mean", "Mean of Gaussian", 5279,5200, 5350);
    RooRealVar *mass_sigma1    = new RooRealVar ("massSignal_sigma1", "Sigma1 of DoubleGaussian", 20, 1, 30);
    RooRealVar *mass_sigma2    = new RooRealVar ("massSignal_sigma2", "Sigma2 of DoubleGaussian",40, 1, 70);
    RooRealVar *mass_fraction  = new RooRealVar ("massSignal_fraction", "Fraction of DoubleGaussian",0.5, 0., 1.);
    RooGaussian *mass_gauss1   = new RooGaussian("massSignal_gauss1", "Gaussian 1", *mass, *mass_mean, *mass_sigma1);
    RooGaussian *mass_gauss2   = new RooGaussian("massSignal_gauss2", "Gaussian 2", *mass, *mass_mean, *mass_sigma2);
    RooAddPdf* mass_gauss = new RooAddPdf  ("mass_gauss", "Mass Signal DoubleGaussian", RooArgList(*mass_gauss1, *mass_gauss2), *mass_fraction, kTRUE);

    RooRealVar *mass_meanMR       = new RooRealVar ("massSignal_meanMR", "", 5358.577);
    RooRealVar *mass_sigmaMR      = new RooRealVar ("massSignal_sigmaMR", "", 51.873);
    RooRealVar *mass_alfaLMR      = new RooRealVar ("massSignal_alfaLMR", "", 1.4586 );
    RooRealVar *mass_alfaRMR      = new RooRealVar ("massSignal_alfaRMR", "", 1.2684);
    RooRealVar *mass_nMR          = new RooRealVar ("massSignal_nMR", "",  6.171);

    RooDSCB *mass_missReco = new RooDSCB("massSignal_missReco","",*mass,*mass_meanMR,*mass_sigmaMR,*mass_alfaLMR,*mass_alfaRMR,*mass_nMR);

    RooRealVar *mass_fractionMR  = new RooRealVar ("massSignal_fractionMR", "Fraction of DoubleGaussian",0.0369);

    RooAddPdf* massSignalPDF = new RooAddPdf  ("massSignalPDF", "Mass Signal DoubleGaussian", RooArgList(*mass_missReco,*mass_gauss), *mass_fractionMR, kTRUE);
    ////Background
    //Mass
    //Mass
    RooRealVar *simpleMass_scale  = new RooRealVar    ("simpleMass_scale", "Scale of Tanh",-0.0179206,-0.18,-0.0018);
    RooRealVar *simpleMass_offset = new RooRealVar    ("simpleMass_offset", "Offset of Tanh", 5141.70,5141.70-10,5141.70+10);
    RooTanhPdf *simpleMass_JpsiX   = new RooTanhPdf    ("simpleMass_JpsiX", "JpisX mass", *mass, *simpleMass_scale, *simpleMass_offset);

    RooRealVar *simpleMass_slope1     = new RooRealVar    ("simpleMass_slope1", "Slope of Exponential1",-8.83178e-04, -1, -1e-8);
    RooExponential *simpleMass_CombinatorialBck = new RooExponential("simpleMass_CombinatorialBck", "CombinatorialBck", *mass, *simpleMass_slope1);
    RooRealVar *simpleMassBck_fraction  = new RooRealVar ("simpleMassBck_fraction", "Fraction of DoubleGaussian",0.90, 0.1, 0.96);

    //Total
    RooRealVar *mass_sigFrac  = new RooRealVar ("mass_sigFrac", "mass_sigFrac",0.44, 0., 1.);
    RooAddPdf* simpleMassPDF = new RooAddPdf  ("massPDF", "massPDF", RooArgList(*massSignalPDF, *simpleMass_CombinatorialBck,*simpleMass_JpsiX), RooArgSet(*mass_sigFrac,*simpleMassBck_fraction), kTRUE);

    //standalone fit
    simpleMassPDF->fitTo(*wdata,RooFit::InitialHesse(kTRUE),RooFit::SumW2Error(kTRUE),NumCPU(nCPU),Timer(kTRUE));
    //Sideband subtraction to prepare punzi

    RooAbsReal* fFitRegion_model = simpleMassPDF->createIntegral(*mass,NormSet(*mass),Range("fitRange")); 
    RooAbsReal* fFitRegion_sig = massSignalPDF->createIntegral(*mass,NormSet(*mass),Range("fitRange")); 

    RooAbsReal* fFitRegion_bck1 = simpleMass_CombinatorialBck->createIntegral(*mass,NormSet(*mass),Range("fitRange")); 
    RooAbsReal* fFitRegion_bck2 = simpleMass_JpsiX->createIntegral(*mass,NormSet(*mass),Range("fitRange"));


    Double_t recalculatedSigFrac         = mass_sigFrac->getValV();// Nsig/Ntot; //ToDo
    Double_t recalculatedMassBckFraction = simpleMassBck_fraction->getValV();// Nbck1/(Nbck1+Nbck2);

    cout << recalculatedSigFrac << " vs. " << mass_sigFrac->getValV()<<endl;
    cout << recalculatedMassBckFraction << " vs. " << simpleMassBck_fraction->getValV()<<endl;

    mass->setRange("signal",mass_mean->getValV()-signaCutoff,mass_mean->getValV()+signaCutoff);
    mass->setRange("leftSideband",massFitMin,mass_mean->getValV()-sidebandCutoff);
    mass->setRange("rightSideband",mass_mean->getValV()+sidebandCutoff,massMax);
   
    RooAbsReal* fsigRegion_model = simpleMassPDF->createIntegral(*mass,NormSet(*mass),Range("signal")); 
    RooAbsReal* fsigRegion_sig = massSignalPDF->createIntegral(*mass,NormSet(*mass),Range("signal")); 

    RooAbsReal* fsigRegion_bck1 = simpleMass_CombinatorialBck->createIntegral(*mass,NormSet(*mass),Range("signal")); 
    RooAbsReal* fsigRegion_bck2 = simpleMass_JpsiX->createIntegral(*mass,NormSet(*mass),Range("signal")); 

    RooAbsReal* fbckRegion_bck1;
    RooAbsReal* fbckRegion_bck2;
    if(backgroundPunziSubtractTime){
    fbckRegion_bck1 = simpleMass_CombinatorialBck->createIntegral(*mass,NormSet(*mass),Range("leftSideband")); 
    fbckRegion_bck2 = simpleMass_JpsiX->createIntegral(*mass,NormSet(*mass),Range("leftSideband")); 
    }
    else{
    fbckRegion_bck1 = simpleMass_CombinatorialBck->createIntegral(*mass,NormSet(*mass),Range("leftSideband,rightSideband")); 
    fbckRegion_bck2 = simpleMass_JpsiX->createIntegral(*mass,NormSet(*mass),Range("leftSideband,rightSideband"));     
    }
    Double_t    fsig = mass_sigFrac->getValV();
    Double_t    fbck1 = (1-fsig)*simpleMassBck_fraction->getValV();
    Double_t    fbck2 = (1-fsig)*(1-simpleMassBck_fraction->getValV());

    Double_t fbckSideband = simpleMassBck_fraction->getValV()*fbckRegion_bck1->getValV()/(simpleMassBck_fraction->getValV()*fbckRegion_bck1->getValV()+(1-simpleMassBck_fraction->getValV())*fbckRegion_bck2->getValV());
    cout <<"Fraction of exponential in left sideband: "<< fbckSideband<<endl;

    RooDataSet* dataSidebandBoth = (RooDataSet*)wdataFit->reduce(Form("B_mass<%f || B_mass>%f",mass_mean->getValV()-sidebandCutoff,mass_mean->getValV()+sidebandCutoff));
    dataSidebandBoth->SetName("dataSidebandBoth");
    RooDataSet* dataSidebandLeft = (RooDataSet*)wdataFit->reduce(Form("B_mass<%f",mass_mean->getValV()-sidebandCutoff));
    dataSidebandLeft->SetName("dataSidebandLeft");
    RooDataSet* dataSidebandRight = (RooDataSet*)wdataFit->reduce(Form("B_mass>%f",mass_mean->getValV()+sidebandCutoff));
    dataSidebandRight->SetName("dataSidebandRight");
    RooDataSet* dataSignalBand = (RooDataSet*)wdataFit->reduce(Form("B_mass>%f && B_mass<%f",mass_mean->getValV()-signaCutoff,mass_mean->getValV()+signaCutoff));
    dataSignalBand->SetName("dataSignalBand");

    TH1F* signal_hist     = (TH1F*)dataSignalBand->createHistogram("signal_hist",*timeErr,Binning(nBins,tauErrMin, tauErrMax));
    TH1F* background_histLeft = (TH1F*)dataSidebandLeft->createHistogram("background_histLeft",*timeErr,Binning(nBins,tauErrMin, tauErrMax));
    TH1F* background_histRight = (TH1F*)dataSidebandRight->createHistogram("background_histRight",*timeErr,Binning(nBins,tauErrMin, tauErrMax));
    TH1F* background_histBoth = (TH1F*)dataSidebandBoth->createHistogram("background_histBoth",*timeErr,Binning(nBins,tauErrMin, tauErrMax));
    signal_hist->Scale(1.0/signal_hist->Integral(), "width");
    background_histLeft->Scale(1.0/background_histLeft->Integral(), "width");
    background_histRight->Scale(1.0/background_histRight->Integral(), "width");
    background_histBoth->Scale(1.0/background_histBoth->Integral(), "width");

    TH1F* background_histTimeErrCombinatorial;
    TH1F* background_histTimeErrMissReco;

    if(backgroundPunziSubtractTime){
        background_histLeft->Add(background_histRight,-1*fbckSideband);
        background_histLeft->Scale(1.0/background_histLeft->Integral(), "width");
        signal_hist->Add(background_histRight,-1*fbck1);
        signal_hist->Add(background_histLeft,-1*fbck2);
        background_histTimeErrCombinatorial = background_histRight;
        background_histTimeErrMissReco = background_histLeft;
    }
    else{
        signal_hist->Add(background_histBoth,-1*fbck1);
        signal_hist->Add(background_histLeft,-1*fbck2);
        background_histTimeErrCombinatorial = background_histBoth;
        background_histTimeErrMissReco = background_histLeft;
    }
 
    TH1F* signal_histMassErr     = (TH1F*)dataSignalBand->createHistogram("signal_histMassErr",*massErr,Binning(nBins,massErrMin,massErrMax));
    TH1F* background_histMassErrLeft = (TH1F*)dataSidebandLeft->createHistogram("background_histMassErrLeft",*massErr,Binning(nBins,massErrMin,massErrMax));
    TH1F* background_histMassErrRight = (TH1F*)dataSidebandRight->createHistogram("background_histMassErrRight",*massErr,Binning(nBins,massErrMin,massErrMax));
    TH1F* background_histMassErrBoth = (TH1F*)dataSidebandBoth->createHistogram("background_histMassErrBoth",*massErr,Binning(nBins,massErrMin,massErrMax));
    signal_histMassErr->Scale(1.0/signal_histMassErr->Integral(), "width");
    background_histMassErrLeft->Scale(1.0/background_histMassErrLeft->Integral(), "width");
    background_histMassErrRight->Scale(1.0/background_histMassErrRight->Integral(), "width");
    background_histMassErrBoth->Scale(1.0/background_histMassErrBoth->Integral(), "width");
    
    TH1F* background_histMassErrCombinatorial;
    TH1F* background_histMassErrMissReco;
    
    if(backgroundPunziSubtractMass){
    background_histMassErrLeft->Add(background_histMassErrRight,-1*fbckSideband);
    background_histMassErrLeft->Scale(1.0/background_histMassErrLeft->Integral(), "width");
    signal_histMassErr->Add(background_histMassErrLeft,-1*fbck2);
    signal_histMassErr->Add(background_histMassErrRight,-1*fbck1);
    background_histMassErrCombinatorial = background_histMassErrRight;
    background_histMassErrMissReco = background_histMassErrLeft;
    }else{
        signal_histMassErr->Add(background_histMassErrLeft,-1*fbck2);
        signal_histMassErr->Add(background_histMassErrBoth,-1*fbck1);
        background_histMassErrCombinatorial = background_histMassErrBoth;
        background_histMassErrMissReco = background_histMassErrLeft;
    }
    
    TH1F* signal_histPt     = (TH1F*)dataSignalBand->createHistogram("signal_histPt",*pt,Binning(nBins,ptMin,ptMax));
    TH1F* background_histPtLeft = (TH1F*)dataSidebandLeft->createHistogram("background_histMassPtLeft",*pt,Binning(nBins,ptMin,ptMax));
    TH1F* background_histPtRight = (TH1F*)dataSidebandRight->createHistogram("background_histMassPtRight",*pt,Binning(nBins,ptMin,ptMax));
    signal_histPt->Scale(1.0/signal_histPt->Integral(), "width");
    background_histPtLeft->Scale(1.0/background_histPtLeft->Integral(), "width");
    background_histPtRight->Scale(1.0/background_histPtRight->Integral(), "width");
    background_histPtLeft->Add(background_histPtRight,-1*fbckSideband);
    background_histPtLeft->Scale(1.0/background_histPtLeft->Integral(), "width");
    signal_histPt->Add(background_histPtLeft,-1*(1-fsig)*fbck2);
    signal_histPt->Add(background_histPtRight,-1*(1-fsig)*fbck1);
 
    //Create Punzi PDF from the histograms
    RooDataHist* background_dataHistLeft = new RooDataHist(" background_dataHistLeft", " background_dataHist", *timeErr, background_histTimeErrMissReco);
    RooHistPdf* backgroundPunziPDFLeft = new RooHistPdf("backgroundPunziPDFLeft","backgroundPunziPDF", *timeErr, *background_dataHistLeft);

    RooDataHist* background_dataHistRight = new RooDataHist(" background_dataHistRight", " background_dataHist", *timeErr, background_histTimeErrCombinatorial);
    RooHistPdf* backgroundPunziPDFRight = new RooHistPdf("backgroundPunziPDFRight","backgroundPunziPDF", *timeErr, *background_dataHistRight);
    
    RooDataHist* signal_dataHist = new RooDataHist(" signal_dataHist", " signal_dataHist", *timeErr, signal_hist);
    RooHistPdf* signalPunziPDF = new RooHistPdf("signalPunziPDF","signalPunziPDF", *timeErr, *signal_dataHist);

    RooAddPdf *totPunziPdf = new RooAddPdf  ("totPunziPdf", "total punzi pdf", RooArgList(*signalPunziPDF, *backgroundPunziPDFRight,*backgroundPunziPDFLeft),RooArgList(*mass_sigFrac,*simpleMassBck_fraction), kTRUE);
    
    RooDataHist* background_dataHistMassErrMissReco = new RooDataHist(" background_dataHistMassErrMissReco", " background_dataHist", *massErr, background_histMassErrMissReco);
    RooHistPdf* backgroundPunziPDFMassErrMissReco = new RooHistPdf("backgroundPunziPDFMassErrMissReco","backgroundPunziPDF", *massErr, *background_dataHistMassErrMissReco);

    RooDataHist* background_dataHistMassErrCombinatorial = new RooDataHist(" background_dataHistMassErrCombinatorial", " background_dataHist", *massErr, background_histMassErrCombinatorial);
    RooHistPdf* backgroundPunziPDFMassErrCombinatorial = new RooHistPdf("backgroundPunziPDFMassErrCombinatorial","backgroundPunziPDF", *massErr, *background_dataHistMassErrCombinatorial);

    RooDataHist* signal_dataHistMassErr = new RooDataHist(" signal_dataHistMassErr", " signal_dataHist", *massErr, signal_histMassErr);
    RooHistPdf* signalPunziPDFMassErr = new RooHistPdf("signalPunziPDFMassErr","signalPunziPDF", *massErr, *signal_dataHistMassErr);

    RooAddPdf* totPunziPdfMassErr = new RooAddPdf  ("totPunziPdfMassErr", "total punzi pdf", RooArgList(*signalPunziPDFMassErr, *backgroundPunziPDFMassErrCombinatorial,*backgroundPunziPDFMassErrMissReco),RooArgList(*mass_sigFrac,*simpleMassBck_fraction), kTRUE);

    RooDataHist* background_dataHistPtLeft = new RooDataHist(" background_dataHistPtLeft", " background_dataHist", *pt, background_histPtLeft);
    RooHistPdf* backgroundPunziPDFPtLeft = new RooHistPdf("backgroundPunziPDFPtLeft","backgroundPunziPDF", *pt, *background_dataHistPtLeft);

    RooDataHist* background_dataHistPtRight = new RooDataHist(" background_dataHistPtRight", " background_dataHist", *pt, background_histPtRight);
    RooHistPdf* backgroundPunziPDFPtRight = new RooHistPdf("backgroundPunziPDFPtRight","backgroundPunziPDF", *pt, *background_dataHistPtRight);

    RooDataHist* signal_dataHistPt = new RooDataHist(" signal_dataHistPt", " signal_dataHist", *pt, signal_histPt);
    RooHistPdf* signalPunziPDFPt = new RooHistPdf("signalPunziPDFPt","signalPunziPDF", *pt, *signal_dataHistPt);

    RooAddPdf *totPunziPdfPt = new RooAddPdf  ("totPunziPdfPt", "total punzi pdf", RooArgList(*signalPunziPDFPt, *backgroundPunziPDFPtRight, *backgroundPunziPDFPtLeft),RooArgList(*mass_sigFrac,*simpleMassBck_fraction), kTRUE);

    TCanvas *c = new TCanvas("c","",3*800,3*600);
    TPad* pads[12];
    for(int i = 0;i<3;i++){
        pads[i]   = new TPad(Form("pad%d",i),   "",i*0.3,1.0,0.3*(i+1),0.7);
        pads[3+i] = new TPad(Form("pad%d",3+i), "",i*0.3,0.7,0.3*(i+1),0.4);
        pads[6+i] = new TPad(Form("pad%d",6+i), "",i*0.3,0.4,0.3*(i+1),0.4*0.2);
        pads[9+i] = new TPad(Form("pad%d",9+i), "",i*0.3,0.4*0.2,0.3*(i+1),0.0);
    }
    for(int i = 0;i<12;i++)pads[i]->Draw();

    RooPlot* simpleMassFrame    = (RooPlot*)mass->frame();
    RooPlot* timeErrFrame = (RooPlot*)timeErr->frame();
    RooPlot* massErrFrame = (RooPlot*)massErr->frame();
    RooPlot* ptFrame = (RooPlot*)pt->frame();

    wdata->plotOn(simpleMassFrame,DataError(RooAbsData::SumW2));
    simpleMassPDF->plotOn(simpleMassFrame,RooFit::Components("mass_gauss"),LineColor(2));
    simpleMassPDF->plotOn(simpleMassFrame,RooFit::Components("massSignal_missReco"),LineColor(6));
    simpleMassPDF->plotOn(simpleMassFrame,RooFit::Components("simpleMass_JpsiX"),LineColor(1));
    simpleMassPDF->plotOn(simpleMassFrame,RooFit::Components("simpleMass_CombinatorialBck"),LineColor(3));
    simpleMassPDF->plotOn(simpleMassFrame);
    wdataFit->plotOn(timeErrFrame,DataError(RooAbsData::SumW2));
    totPunziPdf->plotOn(timeErrFrame);
    totPunziPdf->plotOn(timeErrFrame,RooFit::Components("signalPunziPDF"),LineColor(2));
    totPunziPdf->plotOn(timeErrFrame,RooFit::Components("backgroundPunziPDFLeft"),LineColor(1));
    totPunziPdf->plotOn(timeErrFrame,RooFit::Components("backgroundPunziPDFRight"),LineColor(3));
    wdataFit->plotOn(massErrFrame,DataError(RooAbsData::SumW2));
    totPunziPdfMassErr->plotOn(massErrFrame);
    totPunziPdfMassErr->plotOn(massErrFrame,RooFit::Components("signalPunziPDFMassErr"),LineColor(2));
    totPunziPdfMassErr->plotOn(massErrFrame,RooFit::Components("backgroundPunziPDFMassErrMissReco"),LineColor(1));
    totPunziPdfMassErr->plotOn(massErrFrame,RooFit::Components("backgroundPunziPDFMassErrCombinatorial"),LineColor(3));
    wdataFit->plotOn(ptFrame,DataError(RooAbsData::SumW2));
    totPunziPdfPt->plotOn(ptFrame);
    totPunziPdfPt->plotOn(ptFrame,RooFit::Components("signalPunziPDFPt"),LineColor(2));
    totPunziPdfPt->plotOn(ptFrame,RooFit::Components("backgroundPunziPDFPtLeft"),LineColor(1));
    totPunziPdfPt->plotOn(ptFrame,RooFit::Components("backgroundPunziPDFPtRight"),LineColor(3));

    pads[0]->cd();
    simpleMassFrame->Draw();
    pads[1]->cd();
    timeErrFrame->Draw();
    pads[2]->cd();
    massErrFrame->Draw();
    pads[3]->cd();
    ptFrame->Draw();
    if(years.find(year) != years.end())c->SaveAs(Form("../run/BplusMassLifetime%s.png",year.Data()));
    else if(year == "2016") c->SaveAs("../run/BplusMassLifetime2016.png");
    else c->SaveAs("../run/BplusMassLifetimeFullRun2.png");

    ///Prepare mass-lifetimemodel
    
    //Mass Resolution
    RooRealVar *massPCE_mean     = new RooRealVar   ("massPCE_mean", "Mean (0.0) of Mass Resolution", mass_mean->getValV(),5250,5300);
    RooRealVar *massPCE_SF 	= new RooRealVar   ("massPCE_SF", "SF of Mass Error", 1.28, 0.1, 10.);
    RooProduct *massPCE_sigma       = new RooProduct("massPCE_sigma","massErr * Scale Factor", RooArgList(*massErr, *massPCE_SF));
    RooGaussian *massResolutionPDF  = new RooGaussian("massResolution", "Mass Resolution Gaussian (for both Sig and Bck)", *mass, *massPCE_mean,*massPCE_sigma);

    //Time resolution
    RooRealVar *timeResolution_mean               = new RooRealVar   ("timeResolution_mean", "Mean (0.0) of Time Resolution", 0.);
    //RooRealVar *timeResolution_sigma = new RooRealVar   ("timeResolution_sigma", "SF of Time Error", 0.1, 1e-8, 10.);
    RooRealVar *timeResolution_SF = new RooRealVar   ("timeResolution_SF", "SF of Time Error", 1.11, 0.1, 10.);
    RooGaussModel *timeResolutionPDF  = new RooGaussModel("timeResolutionPDF", "Lifetime Resolution Gaussian (for both Sig and Bck)", *time, *timeResolution_mean,*timeResolution_SF,*timeErr);

    ////Signal
    //Mass
    RooProdPdf* massSignalPDFCond = new RooProdPdf("massSignalPDFCond","massSignalPDFCond",*signalPunziPDFMassErr,Conditional(*massResolutionPDF,*mass)) ;
    RooProdPdf* massMissRecoPDFCond = new RooProdPdf("massMissRecoPDFCond","massMissRecoPDFCond",*signalPunziPDFMassErr,Conditional(*mass_missReco,*mass)) ;

    RooAddPdf* massSigPDF = new RooAddPdf  ("massSigPDF", "Mass Signal PDF", RooArgList(*massMissRecoPDFCond,*massSignalPDFCond),*mass_fractionMR, kTRUE);
    //Time
    RooRealVar *timeSignal_tau = new RooRealVar("timeSignal_tau","timeSignal_tau",1.639,1.5,1.8) ;
    RooDecay* timeSignalPDF =  new RooDecay("signalTime","signalTime",*time,*timeSignal_tau,*timeResolutionPDF,RooDecay::SingleSided) ;
    RooProdPdf* timeSignalPDFCond = new RooProdPdf("timeSignalPDFCond","timeSignalPDFCond",*signalPunziPDF,Conditional(*timeSignalPDF,*time)) ;

    RooProdPdf *signalPDF;
    if(useMassPCE){
        signalPDF = new RooProdPdf("signalPDF","signalPDF",RooArgSet(*massSigPDF,*timeSignalPDFCond));
    }else{
        signalPDF = new RooProdPdf("signalPDF","signalPDF",RooArgSet(*massSignalPDF,*timeSignalPDFCond));
    }    ////Background
    //Mass
    RooRealVar *mass_scale  = new RooRealVar    ("mass_scale", "Scale of Tanh",simpleMass_scale->getValV(),simpleMass_scale->getValV()-4*simpleMass_scale->getError(),simpleMass_scale->getValV()+4*simpleMass_scale->getError());
    RooRealVar *mass_offset = new RooRealVar    ("mass_offset", "Offset of Tanh",simpleMass_offset->getValV(), simpleMass_offset->getValV()-2*simpleMass_offset->getError(),simpleMass_offset->getValV()+2*simpleMass_offset->getError());
    
    RooTanhPdf *mass_JpsiX  = new RooTanhPdf    ("mass_JpsiX", "JpsiX", *mass, *mass_scale, *mass_offset);

    RooRealVar *mass_slope1     = new RooRealVar    ("massBck_slope1", "Slope of Exponential1",simpleMass_slope1->getValV(), -1, -1e-8);
    RooExponential *mass_CombinatorialBck = new RooExponential("mass_CombinatorialBck", "mass_CombinatorialBck", *mass, *mass_slope1);
    RooRealVar *massBck_fraction  = new RooRealVar ("massBck_fraction", "Fraction of DoubleGaussian",recalculatedMassBckFraction, 0., 1.);


    RooProdPdf* mass_CombinatorialBck_cond = new RooProdPdf("mass_CombinatorialBck_cond","mass_CombinatorialBck_cond",*backgroundPunziPDFMassErrCombinatorial,Conditional(*mass_CombinatorialBck,*mass)) ;
    RooProdPdf* mass_JpsiX_cond = new RooProdPdf("mass_JpsiX_cond","mass_JpsiX_cond",*backgroundPunziPDFMassErrMissReco,Conditional(*mass_JpsiX,*mass));
    
    //Time
    RooRealVar *timeBck_tauPos1      = new RooRealVar("timeBck_tauPos1", "Bck Tau Pos 1", 0.853, .05, 1.9);
    RooRealVar *timeBck_tauPos2      = new RooRealVar("timeBck_tauPos2", "Bck Tau Pos 2", 0.125, .05, 1.9);
    RooRealVar *timeBck_tauNeg       = new RooRealVar("timeBck_tauNeg",  "Bck Tau Neg",   1.10029e-01, 1e-4, 1.0);
    RooRealVar *timeBck_tauNeg2      = new RooRealVar("timeBck_tauNeg2",  "Bck Tau Neg2", 2.10029e-01, 1e-4, 1.0);
    RooRealVar *timeBck_fractionPos1 = new RooRealVar("timeBck_fractionPos1", "Fraction of PositiveExpo1",8.27597e-02, 0., 1.);
    RooRealVar *timeBck_fractionPos2 = new RooRealVar("timeBck_fractionPos2", "Fraction of PositiveExpo2",2.30404e-01, 0., 1.);
    RooRealVar *timeBck_fractionNeg  = new RooRealVar("timeBck_fractionNeg",  "Fraction of NegativeExpo",2.61591e-02,  0., 1.);
    RooRealVar *timeBck_fractionNeg2 = new RooRealVar("timeBck_fractionNeg2",  "Fraction of NegativeExpo2",2.61591e-02,  0., 1.);
    RooDecay *timeBck_decayPos1      = new RooDecay  ("timeBck_decayPos1", "Decay Pos 1", *time, *timeBck_tauPos1, *timeResolutionPDF, RooDecay::SingleSided);
    RooDecay *timeBck_decayPos2      = new RooDecay  ("timeBck_decayPos2", "Decay Pos 2", *time, *timeBck_tauPos2, *timeResolutionPDF, RooDecay::SingleSided);
    RooDecay *timeBck_decayNeg       = new RooDecay  ("timeBck_decayNeg",  "Decay Neg",   *time, *timeBck_tauNeg,  *timeResolutionPDF, RooDecay::Flipped);
    RooDecay *timeBck_decayNeg2      = new RooDecay  ("timeBck_decayNeg2",  "Decay Neg2",   *time, *timeBck_tauNeg2,  *timeResolutionPDF, RooDecay::Flipped);

    RooAddPdf* timeBckPDFCombinatorial      = new RooAddPdf ("timeBckPDFCombinatorial", "Lifetime Background (Prompt+2NegativeExpo+2PositiveExpo)", RooArgList(*timeBck_decayPos1, *timeBck_decayPos2, *timeBck_decayNeg, *timeBck_decayNeg2, *timeResolutionPDF), RooArgList(*timeBck_fractionPos1, *timeBck_fractionPos2, *timeBck_fractionNeg, *timeBck_fractionNeg2), kTRUE);

    RooRealVar *timeBck_tauJpsiX      = new RooRealVar("timeBck_tauJpsiX", "Bck Tau Pos Left", 1.36316, 0.004, 4.0);
    RooDecay *timeBck_JpsiX      = new RooDecay  ("timeBck_JpsiX", "Decay Pos Left", *time, *timeBck_tauJpsiX, *timeResolutionPDF, RooDecay::SingleSided);

    RooAddPdf* timeBckPDF = new RooAddPdf ("timeBckPDF", "Lifetime Background Left+Right", RooArgList(*timeBckPDFCombinatorial,*timeBck_JpsiX),*massBck_fraction);

    RooProdPdf* timeResolutionPDF_cond = new RooProdPdf("timeResolutionPDF_cond","timeBck_decayPos1_cond",*backgroundPunziPDFRight,Conditional(*timeResolutionPDF,*time)) ;
    RooProdPdf* timeBck_decayPos1_cond = new RooProdPdf("timeBck_decayPos1_cond","timeBck_decayPos1_cond",*backgroundPunziPDFRight,Conditional(*timeBck_decayPos1,*time)) ;
    RooProdPdf* timeBck_decayPos2_cond = new RooProdPdf("timeBck_decayPos2_cond","timeBck_decayPos1_cond",*backgroundPunziPDFRight,Conditional(*timeBck_decayPos2,*time)) ;
    RooProdPdf* timeBck_decayNeg_cond  = new RooProdPdf("timeBck_decayNeg_cond","timeBck_decayPos1_cond" ,*backgroundPunziPDFRight,Conditional(*timeBck_decayNeg,*time)) ;
    RooProdPdf* timeBck_decayNeg2_cond = new RooProdPdf("timeBck_decayNeg2_cond","timeBck_decayPos1_cond",*backgroundPunziPDFRight,Conditional(*timeBck_decayNeg2,*time)) ;
    RooAddPdf* timeBckPDFCombinatorial_cond      = new RooAddPdf ("timeBckPDFCombinatorial_cond", "Lifetime Background (Prompt+2NegativeExpo+2PositiveExpo)", RooArgList(*timeBck_decayPos1_cond, *timeBck_decayPos2_cond, *timeBck_decayNeg_cond, *timeBck_decayNeg2_cond, *timeResolutionPDF_cond), RooArgList(*timeBck_fractionPos1, *timeBck_fractionPos2, *timeBck_fractionNeg, *timeBck_fractionNeg2), kTRUE);
    
    RooProdPdf* timeBck_JpsiX_cond = new RooProdPdf("timeBck_JpsiX_cond","timeBck_JpsiX_cond",*backgroundPunziPDFLeft,Conditional(*timeBck_JpsiX,*time)) ;
    
    RooRealVar *massBck_fractionLeft  = new RooRealVar ("massBck_fractionLeft", "Fraction of DoubleGaussian",fbckSideband);

    RooAbsPdf* timeBckPDFCond= new RooAddPdf ("timeBckPDFCond", "Lifetime Background Left+Right", RooArgList(*timeBckPDFCombinatorial_cond,*timeBck_JpsiX_cond),*massBck_fraction);
    RooAbsPdf* timeBckPDFCondLeft = new RooAddPdf ("timeBckPDFCondLeft", "Lifetime Background Left+Right", RooArgList(*timeBckPDFCombinatorial_cond,*timeBck_JpsiX_cond),*massBck_fractionLeft);
   

//background lifetime prefit
    if(useWeight)timeBckPDFCombinatorial_cond->fitTo(*dataSidebandRight,NumCPU(nCPU),SumW2Error(kTRUE));
    else timeBckPDFCombinatorial_cond->fitTo(*dataSidebandRight,NumCPU(nCPU));
    
    RooPlot* timePrefitFrame = (RooPlot*)time->frame(Range(-2,12));
    dataSidebandRight->plotOn(timePrefitFrame,DataError(RooAbsData::SumW2));
    timeBckPDFCombinatorial_cond->plotOn(timePrefitFrame);
    pads[4]->cd();
    gPad->SetLogy();
    timePrefitFrame->Draw();
    timePrefitFrame->SetMinimum(1);
    if(years.find(year) != years.end())c->SaveAs(Form("../run/BplusMassLifetime%s.png",year.Data()));
    else if(year == "2016") c->SaveAs("../run/BplusMassLifetime2016.png");
    else c->SaveAs("../run/BplusMassLifetimeFullRun2.png");



    timeBck_tauPos1->setConstant(kTRUE);
    timeBck_tauPos2->setConstant(kTRUE);
    timeBck_tauNeg->setConstant(kTRUE);
    timeBck_tauNeg2->setConstant(kTRUE);

    //timeBck_fractionPos1->setConstant(kTRUE);
    //timeBck_fractionPos2->setConstant(kTRUE);
    //timeBck_fractionNeg->setConstant(kTRUE);
    //timeBck_fractionNeg2->setConstant(kTRUE);

//    timeSignal_tau->setConstant(kTRUE);
//    mass_scale->setConstant(kTRUE);

    massBck_fractionLeft->setConstant(kTRUE);

    if(useWeight)timeBckPDFCondLeft->fitTo(*dataSidebandBoth,NumCPU(nCPU),SumW2Error(kTRUE));
    else timeBckPDFCondLeft->fitTo(*dataSidebandBoth,NumCPU(nCPU));

//    timeBck_tauPos1->setConstant(kTRUE);
//    timeBck_tauPos2->setConstant(kTRUE);
//    timeBck_tauNeg->setConstant(kTRUE);
//    timeBck_tauNeg2->setConstant(kTRUE);

//    timeBck_fractionPos1->setConstant(kTRUE);
//    timeBck_fractionPos2->setConstant(kTRUE);
    //timeBck_fractionNeg->setConstant(kTRUE);
    //timeBck_fractionNeg2->setConstant(kTRUE);

    RooPlot* timePrefitFrame2 = (RooPlot*)time->frame(Range(-2,18));
    dataSidebandBoth->plotOn(timePrefitFrame2,DataError(RooAbsData::SumW2));
    timeBckPDFCondLeft->plotOn(timePrefitFrame2);

    pads[5]->cd();
    gPad->SetLogy();
    timePrefitFrame2->Draw();
    timePrefitFrame2->SetMinimum(1);
    if(years.find(year) != years.end())c->SaveAs(Form("../run/BplusMassLifetime%s.png",year.Data()));
    else if(year == "2016") c->SaveAs("../run/BplusMassLifetime2016.png");
    else c->SaveAs("../run/BplusMassLifetimeFullRun2.png");

    timeBck_tauJpsiX->setConstant(kTRUE);

RooProdPdf *combinatorialBackgroundPDF;
RooProdPdf *JpsiXBackgroundPDF;
    if(useMassPCE){
        combinatorialBackgroundPDF = new RooProdPdf("combinatorialBackgroundPDF","combinatorialBackgroundPDF",RooArgSet(*mass_CombinatorialBck_cond,*timeBckPDFCombinatorial_cond));
        JpsiXBackgroundPDF = new RooProdPdf("JpsiXBackgroundPDF","JpsiXBackgroundPDF",RooArgSet(*mass_JpsiX_cond,*timeBck_JpsiX_cond));
    }else{
        combinatorialBackgroundPDF = new RooProdPdf("combinatorialBackgroundPDF","combinatorialBackgroundPDF",RooArgSet(*mass_CombinatorialBck,*timeBckPDFCombinatorial_cond));
        JpsiXBackgroundPDF = new RooProdPdf("JpsiXBackgroundPDF","JpsiXBackgroundPDF",RooArgSet(*mass_JpsiX,*timeBck_JpsiX_cond));
    }
    //Total

    RooRealVar *sigFrac  = new RooRealVar ("sigFrac", "sigFrac",recalculatedSigFrac, 0., 1.);
    sigFrac->setConstant(kTRUE);
    massBck_fraction->setConstant(kTRUE);
    RooAddPdf* totPDF = new RooAddPdf  ("totPDF", "totPDF", RooArgList(*signalPDF, *combinatorialBackgroundPDF,*JpsiXBackgroundPDF), RooArgSet(*sigFrac,*massBck_fraction ), kTRUE);
    
    mass_slope1->setConstant(kTRUE);
    mass_scale->setConstant(kTRUE);
    mass_offset->setConstant(kTRUE);
    
    mass_fraction->setConstant(kTRUE);
    mass_mean->setConstant(kTRUE);
    mass_sigma1->setConstant(kTRUE);
    mass_sigma2->setConstant(kTRUE);

    //Total fit
    if(useWeight)result = totPDF->fitTo(*wdataFit,Save(),NumCPU(nCPU),SumW2Error(kTRUE));
    else result = totPDF->fitTo(*wdataFit,Save(),NumCPU(nCPU));


    //Plot the data to the figure
    //DrawPunziPartofPDF
    RooPlot* massFrame    = (RooPlot*)mass->frame(Range("fitRange"));
    RooPlot* timeFrame = (RooPlot*)time->frame();
    time->setRange("zoom",-0.2,0.2);
    RooPlot* timeZoomFrame = (RooPlot*)time->frame(Range("zoom"));

    wdataFit->plotOn(massFrame,DataError(RooAbsData::SumW2));
    totPDF->plotOn(massFrame,NormRange("fitRange"),RooFit::Components("massSignalPDFCond"),LineColor(2));
    totPDF->plotOn(massFrame,NormRange("fitRange"),RooFit::Components("massMissRecoPDFCond"),LineColor(6));
    //totPDF->plotOn(massFrame,NormRange("fitRange"),RooFit::Components("backgroundPDF"),LineColor(5));
   if(useMassPCE){
        totPDF->plotOn(massFrame,NormRange("fitRange"),RooFit::Components("mass_CombinatorialBck_cond"),LineColor(3));
        totPDF->plotOn(massFrame,NormRange("fitRange"),RooFit::Components("mass_JpsiX_cond"),LineColor(1));
    }else{
        totPDF->plotOn(massFrame,NormRange("fitRange"),RooFit::Components("mass_CombinatorialBck"),LineColor(3));
        totPDF->plotOn(massFrame,NormRange("fitRange"),RooFit::Components("mass_JpsiX"),LineColor(1));
    }
    totPDF->plotOn(massFrame,NormRange("fitRange"));
    wdataFit->plotOn(timeFrame,DataError(RooAbsData::SumW2));
    totPDF->plotOn(timeFrame,RooFit::Components("signalPDF"),LineColor(2));
    totPDF->plotOn(timeFrame,RooFit::Components("backgroundPDF"),LineColor(1));
    totPDF->plotOn(timeFrame);
    wdataFit->plotOn(timeZoomFrame,DataError(RooAbsData::SumW2));
    totPDF->plotOn(timeZoomFrame,RooFit::Components("signalPDF"),LineColor(2));
    totPDF->plotOn(timeZoomFrame,RooFit::Components("backgroundPDF"),LineColor(1));
    totPDF->plotOn(timeZoomFrame);

    RooHist* hpullMass = massFrame->pullHist() ;
    RooPlot* massFramePull = (RooPlot*)mass->frame(Range("fitRange")) ;
    massFramePull->addPlotable(hpullMass,"P") ;

    RooHist* hpullTime = timeFrame->pullHist() ;
    RooPlot* timeFramePull = (RooPlot*)time->frame() ;
    timeFramePull->addPlotable(hpullTime,"P") ;

    RooHist* hpullTimeZoom = timeZoomFrame->pullHist() ;
    RooPlot* timeZoomFramePull = (RooPlot*)time->frame(Range("zoom")) ;
    timeZoomFramePull->addPlotable(hpullTimeZoom,"P") ;

    pads[6]->cd();
    massFrame->Draw();
    pads[7]->cd();
    pads[7]->SetLogy();
    timeFrame->SetMinimum(0.001);
    timeFrame->Draw();
    pads[8]->cd();
    pads[8]->SetLogy();
    timeZoomFrame->SetMinimum(0.001);
    timeZoomFrame->Draw();
    pads[9]->cd();
    massFramePull->Draw();
    pads[10]->cd();
    timeFramePull->Draw();
    pads[11]->cd();
    timeZoomFramePull->Draw();

    
    result->Print("v");
    cout << "Total number of events:" <<wdata->numEntries() <<endl;	
    if(years.find(year) != years.end())c->SaveAs(Form("../run/BplusMassLifetime%s.png",year.Data()));
    else if(year == "2016") c->SaveAs("../run/BplusMassLifetime2016.png");
    else c->SaveAs("../run/BplusMassLifetimeFullRun2.png");

}
