 #include "RooRealVar.h"
 #include "RooDataSet.h"
 #include "RooDataHist.h"
 #include "RooGaussian.h"
 #include "RooGaussModel.h"
 #include "RooExponential.h"
 #include "TCanvas.h"
 #include "RooPlot.h"
 #include "TTree.h"
 #include "TFile.h"
 #include "TH1D.h"
 #include "TRandom.h"
 #include "RooAddPdf.h"
 #include "RooDecay.h"
 #include "RooProdPdf.h"
 #include "TH1F.h"
 #include "TH2F.h"
 #include "RooHistPdf.h"
 #include "RooFormulaVar.h"
 #include "RooProduct.h"
 #include "TRandom3.h"
 #include "RooHist.h"
 #include "RooFitResult.h"
 #include "RooCategory.h"
 #include "RooWorkspace.h"

 #include "RooPDFs/RooJpsiXPdf.h"
 #include "RooPDFs/RooJpsiXPdf.cxx"
 
 #include "RooPDFs/RooCrystalBallnewgeneral.h"
 #include "RooPDFs/RooCrystalBallnewgeneral.cxx"
 #include "RooJohnson.h"

using namespace RooFit;

    Double_t massMin = 5000;
    Double_t massMax = 5650;

    Double_t tauMin = -2;
    Double_t tauMax = 20;

    Double_t tauErrMin = .018;
    Double_t tauErrMax = .12;

    Double_t massErrMin = 10;
    Double_t massErrMax = 100;

    Double_t ptMin = 10;
    Double_t ptMax = 100;

    set<TString> years {"2015","2016A", "2016B", "2016C", "2017", "2018"};

void setGoodMiddleScaleSymmetric(){
	// Taken from http://ultrahigh.org/2007/08/20/making-pretty-root-color-palettes/
  const Int_t NRGBs = 11;
  const Int_t NCont = 255;
  Double_t stops[NRGBs] = {   0.00, 0.01, 0.10, 0.25, 0.40, 0.50, 0.60, 0.75, 0.90, 0.99, 1.00 };
  Double_t red  [NRGBs] = {   1.00, 1.00, 1.00, 1.00, 0.50, 0.00, 0.50, 1.00, 1.00, 1.00, 1.00 };
  Double_t green[NRGBs] = {   0.00, 0.10, 0.50, 1.00, 1.00, 1.00, 1.00, 1.00, 0.50, 0.10, 0.00 };
  Double_t blue [NRGBs] = {   0.00, 0.00, 0.00, 0.00, 0.50, 0.00, 0.50, 0.00, 0.00, 0.00, 0.00 };
  TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
  gStyle->SetNumberContours(NCont);
}
    
TLegend *drawAtlasLegend(Double_t x1, Double_t x2, Double_t legendTop, Double_t legendSize, Int_t nSteps, Double_t margin = 0.15, Double_t scale = 1.0) {
    
    TLegend *l = new TLegend(x1, legendTop-(nSteps-1)*legendSize*1.36*scale, x2, legendTop+legendSize*0.9);
    l->SetBorderSize(0);
    l->SetFillStyle(0);
    l->SetMargin(margin);
    l->SetTextSize(legendSize);
    l->SetTextFont(42);
    
    return l;
}

void likelihoodScan(TString sgDescription = "dg", TString bgDescription = "expJpsiX", Double_t lifetime = 1.5169, TString tag="s",
TString year = "",bool useWeight = true, bool int sample = -1, bool toyMC = false){
    RooCategory* yearsCategory = new RooCategory("yearsCategory","yearsCategory");
    yearsCategory->defineType("2015" ) ;
    yearsCategory->defineType("2016A") ;
    yearsCategory->defineType("2016B") ;
    yearsCategory->defineType("2016C") ;
    yearsCategory->defineType("2017" ) ;
    yearsCategory->defineType("2018" ) ;

    RooWorkspace* workspace = new RooWorkspace("workspace",kTRUE) ;

    bool useTriggers = true;
    bool useMassPCE = false;
    
    int nBins = 100;
    int nCPU = 8;
    TLegend* l = drawAtlasLegend(0.60, 0.95, 0.85, 0.035, 2); 
    TLegend* l2 = drawAtlasLegend(0.60, 0.95, 0.85, 0.035, 2); 
    //READ the data from file and put them to the RooDataset
    RooFitResult *result;

    RooRealVar *mass = new RooRealVar("B_mass", "B_mass", massMin, massMax);
    RooRealVar *time = new RooRealVar("B_tau_MinA0", "B_tau_MinA0", tauMin, tauMax);
    RooRealVar *massErr = new RooRealVar("B_mass_err", "B_mass_err", massErrMin, massErrMax);
    RooRealVar *timeErr = new RooRealVar("B_tau_MinA0_err", "B_tau_MinA0_err", tauErrMin, tauErrMax);
    RooRealVar *pt = new RooRealVar("B_pT", "B_pT", ptMin, ptMax);
    RooRealVar *weight = new RooRealVar("w", "w", 0.0, 20.0);
    RooArgSet*  vars = new RooArgSet(*mass, *time, *massErr, *timeErr, *pt,*yearsCategory,*weight);

    RooDataSet* wdata = NULL;    
    RooDataSet* dataTmp = NULL;    
    RooDataSet* data = NULL;    
    if(!toyMC){
        TFile* f = new TFile("../../data/RooDatasetBd.root","READ") ;
        dataTmp = (RooDataSet*)f->Get("data"); 
        f->Close();
    

        if(years.find(year) != years.end()){
            data = (RooDataSet*) dataTmp->reduce(*vars,Form("yearsCategory==yearsCategory::%s",year.Data())) ;
        }
        else if(year == "2016"){
        data=(RooDataSet*) dataTmp->reduce(*vars,"yearsCategory==yearsCategory::2016A || yearsCategory==yearsCategory::2016C") ;
        }else{
            //data=(RooDataSet*) dataTmp->reduce(*vars,"yearsCategory==yearsCategory::2015 || yearsCategory==yearsCategory::2016A|| yearsCategory==yearsCategory::2016B|| yearsCategory==yearsCategory::2016C || yearsCategory==yearsCategory::2017 || yearsCategory==yearsCategory::2018") ;
            data=(RooDataSet*) dataTmp->reduce(*vars,"yearsCategory==yearsCategory::2015 || yearsCategory==yearsCategory::2016A || yearsCategory==yearsCategory::2016C || yearsCategory==yearsCategory::2017 || yearsCategory==yearsCategory::2018") ;
        }    
        data->Print();
    }
    else{
            TFile* resultsFile =  new TFile("BdMassLifetimeFitResults.root");
            workspace = (RooWorkspace*)resultsFile->Get("workspace");
            RooAbsPdf *toyPDF = workspace->pdf("totPDF");
            data = (RooDataSet*)toyPDF->generate(*vars,500000);
    }

    if(useWeight){
        wdata = new RooDataSet(data->GetName(),data->GetTitle(),data,*data->get(),0,weight->GetName()) ;
    }
    else wdata = data;

    if (sample > 0) {
    Long64_t nOrig = wdata->numEntries();
    TString title = wdata->GetTitle();
    title += " Random sample ( ";
    title += sample;
    title += " / ";
    title += nOrig;
    title += " )";
    RooDataSet *newDataset = new RooDataSet("wdataRandomSample", title, *vars);
    TRandom3 rand(0);
    for ( Long64_t i = 0; i < sample; i++ ) {
        Long64_t j = rand.Uniform( 0 , nOrig );
        newDataset->add( *(wdata->get( j )) );
    }
    cout << "INFO: Created random sample (" << sample << " from " << nOrig << ")" << endl;
    wdata=newDataset;

    }
    
    ///Prepare model
    ////Signal
    //Mass
    
    
    int nParamsSimple = 0;
    int nParamsTot = 0;
    RooAbsPdf* massSignalPDF;
    RooRealVar *mass_mean      = new RooRealVar ("massSignal_mean", "Mean of Gaussian", 5279,5200, 5350);
    TString niceName;
    cout<<sgDescription<<endl;
    
    RooRealVar *delta ;
    RooRealVar *gamma ;
    RooRealVar *lambda;
    
    if (sgDescription == "cbpe2"){
        RooRealVar *alfaL  = new RooRealVar ("alfaL", "", 1.61509,0.5,8);
        RooRealVar *alfaR  = new RooRealVar ("alfaR", "", 1.94879,0.5,8);
        RooRealVar *nL     = new RooRealVar ("nL", "",  20,1,200);
        RooRealVar *nR     = new RooRealVar ("nR", "",  20,1,200);
        
        RooRealVar *s1L     = new RooRealVar ("s1L", "",   1.25567e+00,0.8,1.35);
        RooRealVar *s2L     = new RooRealVar ("s2L", "",  0.,-0.0025,0.05);
        //general only
        RooRealVar *s1R     = new RooRealVar ("s1R", "",   1.34821e+00,0.8,1.35);
        RooRealVar *s2R     = new RooRealVar ("s2R", "",  0.,-0.0025,0.05);

        RooRealVar *s0L     = new RooRealVar ("s0L", "",  0., -0.02,0.02);
        RooRealVar *s0R     = new RooRealVar ("s0R", "",  0., -0.02,0.02);
        
        s0L->setConstant(kTRUE);
        s0R->setConstant(kTRUE);
        
//         nL->setConstant(kTRUE);
//         nR->setConstant(kTRUE);
//         sgDescription+="fixedN20";
//         nParamsSimple -=2;
        
        massSignalPDF = new RooCrystalBall("massSignalPDF","",*mass,*mass_mean,*massErr,*alfaL,*nL,*alfaR,*nR,*s1L,*s2L, *s1R, *s2R,*s0L,*s0R);  
        nParamsSimple += 9;
        niceName = "Crystal Ball with Quadratic Per-Candidate Error";

    }    if (sgDescription == "cbpe1"){
        RooRealVar *alfaL  = new RooRealVar ("alfaL", "", 1.61509,0.5,8);
        RooRealVar *alfaR  = new RooRealVar ("alfaR", "", 1.94879,0.5,8);
        RooRealVar *nL     = new RooRealVar ("nL", "",  20,1,200);
        RooRealVar *nR     = new RooRealVar ("nR", "",  20,1,200);
        
        RooRealVar *s1L     = new RooRealVar ("s1L", "",   1.25567e+00,0.8,1.35);
        RooRealVar *s2L     = new RooRealVar ("s2L", "",  0.,-0.0025,0.05);
        //general only
        RooRealVar *s1R     = new RooRealVar ("s1R", "",   1.34821e+00,0.8,1.35);
        RooRealVar *s2R     = new RooRealVar ("s2R", "",  0.,-0.0025,0.05);

        RooRealVar *s0L     = new RooRealVar ("s0L", "",  0., -0.02,0.02);
        RooRealVar *s0R     = new RooRealVar ("s0R", "",  0., -0.02,0.02);
        
        s0L->setConstant(kTRUE);
        s0R->setConstant(kTRUE);
        
        s2L->setConstant(kTRUE);
        s2R->setConstant(kTRUE);
        
//         nL->setConstant(kTRUE);
//         nR->setConstant(kTRUE);
//         sgDescription+="fixedN20";
//         nParamsSimple -=2;
        
        massSignalPDF = new RooCrystalBall("massSignalPDF","",*mass,*mass_mean,*massErr,*alfaL,*nL,*alfaR,*nR,*s1L,*s2L, *s1R, *s2R,*s0L,*s0R);  
        nParamsSimple += 7;
        niceName = "Crystal Ball with Linear Per-Candidate Error";

    }else if (sgDescription == "cb"){
        RooRealVar *massErr1     = new RooRealVar ("massErr1", "massErr1",  1.,1.,1.);
        
        RooRealVar *alfaL  = new RooRealVar ("alfaL", "", 1.61509,0.5,8);
        RooRealVar *alfaR  = new RooRealVar ("alfaR", "", 1.94879,0.5,8);
        RooRealVar *nL     = new RooRealVar ("nL", "",  10,1,200);
        RooRealVar *nR     = new RooRealVar ("nR", "",  10,1,200);
        
        RooRealVar *s1L     = new RooRealVar ("s1L", "",   1.25567e+00,0.8,1.35);
        RooRealVar *s2L     = new RooRealVar ("s2L", "",  0.,-0.0025,0.05);
        //general only
        RooRealVar *s1R     = new RooRealVar ("s1R", "",   1.34821e+00,0.8,1.35);
        RooRealVar *s2R     = new RooRealVar ("s2R", "",  0.,-0.0025,0.05);

        RooRealVar *s0L     = new RooRealVar ("s0L", "",  0., -0.02,0.02);
        RooRealVar *s0R     = new RooRealVar ("s0R", "",  0., -0.02,0.02);
        
        s1L->setRange(0.5,100);
        s1R->setRange(0.5,100);
        
        s0L->setConstant(kTRUE);
        s0R->setConstant(kTRUE);
        s2L->setConstant(kTRUE);
        s2R->setConstant(kTRUE);
        
        nL->setConstant(kTRUE);
        nR->setConstant(kTRUE);
        sgDescription+="fixedN10";
        niceName = "Crystal Ball";
        nParamsSimple -=2;
//         
        massSignalPDF = new RooCrystalBall("massSignalPDF","",*mass,*mass_mean,*massErr1,*alfaL,*nL,*alfaR,*nR,*s1L,*s2L, *s1R, *s2R,*s0L,*s0R);  
        nParamsSimple += 7;

    }else if(sgDescription =="dg"){
        RooRealVar *mass_sigma1    = new RooRealVar ("massSignal_sigma1", "Sigma1 of DoubleGaussian", 20, 1, 30);
        RooRealVar *mass_sigma2    = new RooRealVar ("massSignal_sigma2", "Sigma2 of DoubleGaussian",40, 1, 70);
        RooRealVar *mass_fraction  = new RooRealVar ("massSignal_fraction", "Fraction of DoubleGaussian",0.5, 0., 1.);
        RooGaussian *mass_gauss1   = new RooGaussian("massSignal_gauss1", "Gaussian 1", *mass, *mass_mean, *mass_sigma1);
        RooGaussian *mass_gauss2   = new RooGaussian("massSignal_gauss2", "Gaussian 2", *mass, *mass_mean, *mass_sigma2);
        
        massSignalPDF = new RooAddPdf  ("massSignalPDF", "Mass Signal DoubleGaussian", RooArgList(*mass_gauss1, *mass_gauss2), *mass_fraction, kTRUE);
        niceName = "Double Gaussian";
        nParamsSimple += 4; //mean, sigma1, sigma2, fraction
    }else if (sgDescription == "john"){
        delta  = new RooRealVar ("delta", "", 4,0,10);
        gamma  = new RooRealVar ("gamma", "",   1.1438e+00,-10,10);
        lambda  = new RooRealVar ("lambda", "", 50,0.0001,5000);
        
        massSignalPDF = new 	RooJohnson(    "massSignalPDF", "Johnson SU", *mass, *mass_mean, *lambda, *gamma, *delta);
        nParamsSimple += 4;
        niceName = "Johnson's Function";
         
//         delta     ->setVal(1.1452e+00);
//         gamma     ->setVal(-1.6247e-03);
//         lambda    ->setVal(3.4259e+01);
//         mass_mean ->setVal(5.2797e+03);
        
//         delta     ->setConstant(kTRUE);
//         gamma     ->setConstant(kTRUE);
// //         lambda    ->setConstant(kTRUE);
// //         mass_mean ->setConstant(kTRUE);
//         nParamsSimple -= 3;
//         sgDescription+="_fixed-1MCfreeLambdaMean";
        
        
    }else if (sgDescription == "jj" || sgDescription == "jjfm"){
        RooRealVar *gamma  = new RooRealVar ("gamma", "", 3,-10,10);
        RooRealVar *delta  = new RooRealVar ("delta", "", 4,0,10);
        RooRealVar *lambda  = new RooRealVar ("lambda", "", 50,0.0001,5000);
        
        delta     ->setVal(1.1452e+00);
        gamma     ->setVal(-1.6247e-03);
        lambda    ->setVal(3.4259e+01);
        mass_mean ->setVal(5.2797e+03);
        
        nParamsSimple += 4;
        
        RooRealVar *gamma2  = new RooRealVar ("gamma2", "", 3,-10,10);
        RooRealVar *delta2  = new RooRealVar ("delta2", "", 4,0,10);
        RooRealVar *lambda2  = new RooRealVar ("lambda2", "", 50,0.0001,5000);
        RooRealVar *mean2 = new RooRealVar ("mean2", "", 5280,5250,5320);
        
        nParamsSimple += 4;
                
        RooJohnson *jSig = new 	RooJohnson(    "JSig", "Johnson Signal Decription", *mass, *mass_mean, *lambda, *gamma, *delta);
        if (sgDescription == "jjfm") mean2 = mass_mean;
        
        RooJohnson *jTune = new 	RooJohnson(    "JTune", "Johnson Tunning", *mass, *mean2, *lambda2, *gamma2, *delta2);
        
        RooRealVar *frac   = new RooRealVar("frac"  , "frac"  ,  0.9,    0.8,    0.999);
        massSignalPDF = new RooAddPdf("massSignalPDF","jj",RooArgList(*jSig,*jTune),*frac);
        nParamsSimple += 1;
         
        niceName = "Double Johnson's Function";
        
        niceName += " Fixed Mass Params"; 
           //jj pol0expexp
        delta   ->setVal( 1.3494e+00 );
        delta2  ->setVal( 4.8184e+00 );
        frac    ->setVal( 8.3542e-01 );
        gamma   ->setVal( -4.0162e-01);
        gamma2  ->setVal( 1.9557e+00 );
        lambda  ->setVal( 4.1707e+01 );
        lambda2 ->setVal( 1.5780e+02 );
        mass_mean ->setVal(5.2727e+03);
        mean2           ->setVal(5.3155e+03);
        if (sgDescription == "jjfm"){
            delta     ->setVal( 1.3739e+00);   
            delta2    ->setVal( 3.2868e+00);   
            frac      ->setVal( 9.4603e-01);   
            gamma     ->setVal(-3.1035e-01);   
            gamma2    ->setVal( 1.9883e+00);   
            lambda    ->setVal( 4.3300e+01);   
            lambda2   ->setVal( 7.3394e+01);   
            mass_mean ->setVal( 5.2726e+03);   
            mean2     ->setVal( 5.2726e+03);          
    }                                            
        delta   ->setConstant(kTRUE);
        delta2  ->setConstant(kTRUE);
        frac    ->setConstant(kTRUE);
        gamma   ->setConstant(kTRUE);
        gamma2  ->setConstant(kTRUE);
        lambda  ->setConstant(kTRUE);
        lambda2 ->setConstant(kTRUE);       
        mass_mean ->setConstant(kTRUE);
        //mass_sigFrac  ->setConstant(kTRUE);
        mean2           ->setConstant(kTRUE);
        
        nParamsSimple -= 8;
    
    }    else {
        cout<<"ERROR: invalid function name"<<endl;
        exit(EXIT_FAILURE);
    }
    nParamsTot=nParamsSimple;

    ////Background
    //Mass
    //Mass
    RooRealVar *simpleMass_slope1     = new RooRealVar    ("simpleMass_slope1", "Slope of Exponential1",-6.83178e-04, -0.1, -1e-8);
    RooRealVar *simpleMass_slope2     = new RooRealVar    ("simpleMass_slope2", "Slope of Exponential2",-9.83178e-04, -0.1, -1e-8);
    RooExponential *simpleMass_exponential1 = new RooExponential("simpleMass_exponential1", "mass_exponential1", *mass, *simpleMass_slope1);
    RooExponential *simpleMass_exponential2 = new RooExponential("simpleMass_exponential2", "mass_exponential2", *mass, *simpleMass_slope2);
    RooRealVar *simpleMassBck_fraction  = new RooRealVar ("simpleMassBck_fraction", "Fraction of DoubleGaussian",0.5, 0.01, 0.99);
    RooRealVar *simpleMassBck_fraction2  = new RooRealVar ("simpleMassBck_fraction2", "Fraction of DoubleGaussian",0.5, 0.01, 0.99);

    RooRealVar *simpleMass_scale     = new RooRealVar    ("simpleMass_scale", "simpleMass_scale",10,1, 20);
    RooRealVar *simpleMass_offset     = new RooRealVar    ("simpleMass_offset", "simpleMass_offset",4750, 4500, 5000);
    RooJpsiXPdf *simpleMass_JpsiX = new RooJpsiXPdf("simpleMass_JpsiX","simpleMass_JpsiX",*mass,*simpleMass_offset,*simpleMass_scale);
    
    RooRealVar *simpleMass_a1     = new RooRealVar    ("simpleMass_a1Pol" , "Linear Term of Pol",-5.0 , -10000., 10000.);
    
//     RooArgList()//const
//     RooArgList(simpleMass_a1)//lin
    RooPolynomial* simpleMass_pol = new RooPolynomial("simpleMass_pol","simpleMass_pol",*mass,RooArgList(*simpleMass_a1)); 
    RooPolynomial* simpleMass_const = new RooPolynomial("simpleMass_pol","simpleMass_pol",*mass,RooArgList()); 
    
    RooAbsPdf* massBckPDF;
    if (bgDescription == "exp"){
    //exp 
        massBckPDF = new RooExponential("massBckPDF", "mass_exponential1", *mass, *simpleMass_slope1);
        nParamsSimple+=1;
    }else if (bgDescription == "expexp"){
    //exp + exp
        massBckPDF = new RooAddPdf  ("massBckPDF", "Mass Background DoubleGaussian", RooArgList(*simpleMass_exponential1, *simpleMass_exponential2),*simpleMassBck_fraction);
        nParamsSimple+=2;
    }else if (bgDescription == "expJpsiX"){
    //exp + tanh
        massBckPDF = new RooAddPdf  ("massBckPDF", "Mass Background DoubleGaussian", RooArgList(*simpleMass_exponential1, *simpleMass_JpsiX),*simpleMassBck_fraction);
        nParamsSimple+=4; //slope1,bgFrac,scale,offset
    
    }else if (bgDescription == "polexp"){
    //exp+pol
        massBckPDF = new RooAddPdf  ("massBckPDF", "Mass Background DoubleGaussian", RooArgList(*simpleMass_pol, *simpleMass_exponential1),*simpleMassBck_fraction);
        nParamsSimple+=3; //slope1,bgFrac,a1
    
    }else if (bgDescription == "polJpsiX"){
    //tanh+pol
        massBckPDF = new RooAddPdf  ("massBckPDF", "Mass Background DoubleGaussian", RooArgList(*simpleMass_pol, *simpleMass_JpsiX),*simpleMassBck_fraction);
        nParamsSimple+=4; //a1,bgFrac,scale,offset
    }else if (bgDescription == "pol0exp"){
    //exp+pol
        massBckPDF = new RooAddPdf  ("massBckPDF", "Mass Background DoubleGaussian", RooArgList(*simpleMass_const, *simpleMass_exponential1),*simpleMassBck_fraction);
        nParamsSimple+=3; //slope1,bgFrac,a1
    
    }else if (bgDescription == "pol0JpsiX"){
    //tanh+pol
        massBckPDF = new RooAddPdf  ("massBckPDF", "Mass Background DoubleGaussian", RooArgList(*simpleMass_const, *simpleMass_JpsiX),*simpleMassBck_fraction);
        nParamsSimple+=4; //a1,bgFrac,scale,offset
    }else if (bgDescription == "pol0expJpsiX"){
    //tanh+pol+exp
        massBckPDF = new RooAddPdf  ("massBckPDF", "Mass Background DoubleGaussian", RooArgList(*simpleMass_const, *simpleMass_JpsiX, *simpleMass_exponential1),RooArgList(*simpleMassBck_fraction,*simpleMassBck_fraction2));
        nParamsSimple+=4; //a1,bgFrac,scale,offset
        
        simpleMassBck_fraction   ->setVal( 1.9518e-01);
        simpleMassBck_fraction2  ->setVal( 2.7699e-01);
        simpleMass_offset        ->setVal( 4.6519e+03);
        simpleMass_scale         ->setVal( 2.3275e+00);
        simpleMass_slope1        ->setVal(-1.8011e-03);


        
        
    }else if (bgDescription == "pol0expexp"){
    //const+exp+exp
        massBckPDF = new RooAddPdf  ("massBckPDF", "Mass Background DoubleGaussian", RooArgList(*simpleMass_const, *simpleMass_exponential1, *simpleMass_exponential2),RooArgList(*simpleMassBck_fraction,*simpleMassBck_fraction2));
        nParamsSimple+=4; //bgFrac,scale,scale frac
        
        simpleMassBck_fraction  ->setVal(9.5700e-02 );
        simpleMassBck_fraction2 ->setVal(7.2378e-01 );
        simpleMass_slope1       ->setVal(-1.2913e-03);
        simpleMass_slope2       ->setVal(-6.0182e-03);

    }else {
        cout<<"ERROR: invalid function name"<<endl;
        exit(EXIT_FAILURE);
    }
    simpleMass_offset       ->setConstant(kTRUE);
    simpleMass_scale        ->setConstant(kTRUE);
    simpleMassBck_fraction  ->setConstant(kTRUE);
    simpleMassBck_fraction2 ->setConstant(kTRUE);
    simpleMass_slope1       ->setConstant(kTRUE);
    simpleMass_slope2       ->setConstant(kTRUE);
    //Total
    RooRealVar *mass_sigFrac  = new RooRealVar ("mass_sigFrac", "mass_sigFrac",0.3, 0., 1.);
//     mass_sigFrac    ->setVal(4.7075e-01);
    mass_sigFrac    ->setVal(     4.6878e-01);
    RooAddPdf* simpleMassPDF = new RooAddPdf  ("massPDF", "massPDF", RooArgList(*massSignalPDF, *massBckPDF), *mass_sigFrac, kTRUE);

    //standalone fit
    //result = simpleMassPDF->fitTo(*wdata,RooFit::InitialHesse(kTRUE),RooFit::SumW2Error(kTRUE),NumCPU(nCPU),Timer(kTRUE),Save());//, Range("full"));
    
    
    //Sideband subtraction to prepare punzi

    mass->setRange("signal",mass_mean->getValV()-80,mass_mean->getValV()+80);
    RooAbsReal* fsigRegion_model = simpleMassPDF->createIntegral(*mass,NormSet(*mass),Range("signal")); 
    RooAbsReal* fsigRegion_sig = massSignalPDF->createIntegral(*mass,NormSet(*mass),Range("signal")); 


    /*cout << wdata->sumEntries()<<endl;
    cout << wdata->sumEntries()*mass_sigFrac->getValV()<<endl;
    cout << wdata->sumEntries()*(1-mass_sigFrac->getValV())<<endl;
    cout << fsigRegion_model->getValV()<<endl;
    cout << fsigRegion_sig->getValV()<<endl;
    cout << wdata->sumEntries()*fsigRegion_model->getValV()<<endl;
    cout << wdata->sumEntries()*mass_sigFrac->getValV()*fsigRegion_sig->getValV()<<endl;*/
    ////Double_t fsig = mass_sigFrac->getValV()*fsigRegion_sig->getValV()/fsigRegion_model->getValV();
    Double_t fsig = mass_sigFrac->getValV();
    RooDataSet* dataSideband = (RooDataSet*)wdata->reduce(Form("B_mass<%f || B_mass>%f",mass_mean->getValV()-120,mass_mean->getValV()+120));
    dataSideband->SetName("dataSideband");
    RooDataSet* dataSignalBand = (RooDataSet*)wdata->reduce(Form("B_mass>%f && B_mass<%f",mass_mean->getValV()-80,mass_mean->getValV()+80));
    dataSignalBand->SetName("dataSignalBand");

    //TH1F* signal_hist     = (TH1F*)dataSignalBand->createHistogram("signal_hist",*timeErr,Binning(nBins,tauErrMin, tauErrMax));
    TH1F* signal_hist     = (TH1F*)wdata->createHistogram("signal_hist",*timeErr,Binning(nBins,tauErrMin, tauErrMax));
    TH1F* background_hist = (TH1F*)dataSideband->createHistogram("background_hist",*timeErr,Binning(nBins,tauErrMin, tauErrMax));
    signal_hist->Scale(1.0/signal_hist->Integral(), "width");
    background_hist->Scale(1.0/background_hist->Integral(), "width");
    signal_hist->Add(background_hist,-1*(1-fsig));
 
    TH1F* signal_histMassErr     = (TH1F*)dataSignalBand->createHistogram("signal_histMassErr",*massErr,Binning(nBins,massErrMin,massErrMax));
    TH1F* background_histMassErr = (TH1F*)dataSideband->createHistogram("background_histMassErr",*massErr,Binning(nBins,massErrMin,massErrMax));
    signal_histMassErr->Scale(1.0/signal_histMassErr->Integral(), "width");
    background_histMassErr->Scale(1.0/background_histMassErr->Integral(), "width");
    signal_histMassErr->Add(background_histMassErr,-1*(1-fsig));
    
    TH1F* signal_histPt     = (TH1F*)dataSignalBand->createHistogram("signal_histPt",*pt,Binning(nBins,ptMin,ptMax));
    TH1F* background_histPt = (TH1F*)dataSideband->createHistogram("background_histMassPt",*pt,Binning(nBins,ptMin,ptMax));
    signal_histPt->Scale(1.0/signal_histPt->Integral(), "width");
    background_histPt->Scale(1.0/background_histPt->Integral(), "width");
    signal_histPt->Add(background_histPt,-1*(1-fsig));
   
    //Create Punzi PDF from the histograms
    RooDataHist* background_dataHist = new RooDataHist(" background_dataHist", " background_dataHist", *timeErr, background_hist);
    RooHistPdf* backgroundPunziPDF = new RooHistPdf("backgroundPunziPDF","backgroundPunziPDF", *timeErr, *background_dataHist);
    
    RooDataHist* signal_dataHist = new RooDataHist(" signal_dataHist", " signal_dataHist", *timeErr, signal_hist);
    RooHistPdf* signalPunziPDF = new RooHistPdf("signalPunziPDF","signalPunziPDF", *timeErr, *signal_dataHist);

    RooAddPdf *totPunziPdf = new RooAddPdf  ("totPunziPdf", "total punzi pdf", RooArgList(*signalPunziPDF, *backgroundPunziPDF),*mass_sigFrac, kTRUE);
    
    RooDataHist* background_dataHistMassErr = new RooDataHist(" background_dataHistMassErr", " background_dataHist", *massErr, background_histMassErr);
    RooHistPdf* backgroundPunziPDFMassErr = new RooHistPdf("backgroundPunziPDFMassErr","backgroundPunziPDF", *massErr, *background_dataHistMassErr);

    RooDataHist* signal_dataHistMassErr = new RooDataHist(" signal_dataHistMassErr", " signal_dataHist", *massErr, signal_histMassErr);
    RooHistPdf* signalPunziPDFMassErr = new RooHistPdf("signalPunziPDFMassErr","signalPunziPDF", *massErr, *signal_dataHistMassErr);

    RooAddPdf *totPunziPdfMassErr = new RooAddPdf  ("totPunziPdfMassErr", "total punzi pdf", RooArgList(*signalPunziPDFMassErr, *backgroundPunziPDFMassErr),*mass_sigFrac, kTRUE);

    RooDataHist* background_dataHistPt = new RooDataHist(" background_dataHistPt", " background_dataHist", *pt, background_histPt);
    RooHistPdf* backgroundPunziPDFPt = new RooHistPdf("backgroundPunziPDFPt","backgroundPunziPDF", *pt, *background_dataHistPt);

    RooDataHist* signal_dataHistPt = new RooDataHist(" signal_dataHistPt", " signal_dataHist", *pt, signal_histPt);
    RooHistPdf* signalPunziPDFPt = new RooHistPdf("signalPunziPDFPt","signalPunziPDF", *pt, *signal_dataHistPt);

    RooAddPdf *totPunziPdfPt = new RooAddPdf  ("totPunziPdfPt", "total punzi pdf", RooArgList(*signalPunziPDFPt, *backgroundPunziPDFPt),*mass_sigFrac, kTRUE);

    ///Prepare mass-lifetimemodel
    
    //Mass Resolution
    RooRealVar *massPCE_mean     = new RooRealVar   ("massPCE_mean", "Mean (0.0) of Mass Resolution", mass_mean->getValV(),5250,5300);
    RooRealVar *massPCE_SF 	= new RooRealVar   ("massPCE_SF", "SF of Mass Error", 1.28, 0.1, 10.);
    RooProduct *massPCE_sigma       = new RooProduct("massPCE_sigma","massErr * Scale Factor", RooArgList(*massErr, *massPCE_SF));
    RooGaussian *massResolutionPDF  = new RooGaussian("massResolution", "Mass Resolution Gaussian (for both Sig and Bck)", *mass, *massPCE_mean,*massPCE_sigma);

    //Time resolution
    RooRealVar *timeResolution_mean               = new RooRealVar   ("timeResolution_mean", "Mean (0.0) of Time Resolution", 0.);
    //RooRealVar *timeResolution_sigma = new RooRealVar   ("timeResolution_sigma", "SF of Time Error", 0.1, 1e-8, 10.);
    RooRealVar *timeResolution_SF = new RooRealVar   ("timeResolution_SF", "SF of Time Error", 1, 0.1, 10.);
    RooGaussModel *timeResolutionPDF  = new RooGaussModel("timeResolutionPDF", "Lifetime Resolution Gaussian (for both Sig and Bck)", *time, *timeResolution_mean,*timeResolution_SF,*timeErr);

     ////Signal
    //Mass
    RooProdPdf* massSignalPDFCond = new RooProdPdf("massSignalPDFCond","massSignalPDFCond",*signalPunziPDFMassErr,Conditional(*massResolutionPDF,*mass)) ;
    //Time
    RooRealVar *timeSignal_tau = new RooRealVar("timeSignal_tau","timeSignal_tau",1.517,1.3,1.7) ;
    RooDecay* timeSignalPDF =  new RooDecay("signalTime","signalTime",*time,*timeSignal_tau,*timeResolutionPDF,RooDecay::SingleSided) ;
    RooProdPdf* timeSignalPDFCond = new RooProdPdf("timeSignalPDFCond","timeSignalPDFCond",*signalPunziPDF,Conditional(*timeSignalPDF,*time)) ;

    //likelihood scan
//     timeSignal_tau->setVal(lifetime);
//     timeSignal_tau->setConstant(kTRUE);
    
    RooProdPdf *signalPDF;
    if(useMassPCE){
        signalPDF = new RooProdPdf("signalPDF","signalPDF",RooArgSet(*massSignalPDFCond,*timeSignalPDFCond));
    }else{
        signalPDF = new RooProdPdf("signalPDF","signalPDF",RooArgSet(*massSignalPDF,*timeSignalPDFCond));
    }    ////Background
    //Mass
    RooRealVar *mass_slope1     = new RooRealVar    ("massBck_slope1", "Slope of Exponential1",simpleMass_slope1->getValV(), -0.1, -1e-8);
    RooRealVar *mass_slope2     = new RooRealVar    ("massBck_slope2", "Slope of Exponential2",simpleMass_slope2->getValV(), -0.1, -1e-8);
    RooExponential *mass_exponential1 = new RooExponential("mass_exponential1", "mass_exponential1", *mass, *mass_slope1);
    RooExponential *mass_exponential2 = new RooExponential("mass_exponential2", "mass_exponential2", *mass, *mass_slope2);
    RooRealVar *massBck_fraction  = new RooRealVar ("massBck_fraction", "Fraction of DoubleGaussian",simpleMassBck_fraction->getValV(), 0., 1.);

    //RooAddPdf* massBckPDF = new RooAddPdf  ("massBckPDF", "Mass Background DoubleGaussian", RooArgList(*mass_exponential1, *mass_exponential2),*massBck_fraction);
    RooProdPdf* massBackgroundPDFCond = new RooProdPdf("massBackgroundPDFCond","massSignalPDFCond",*backgroundPunziPDFMassErr,Conditional(*massBckPDF,*mass)) ;
   
//     RooAddPdf* massBckPDF = massBckPDF;
    
    //Time
    RooRealVar *timeBck_tauPos1      = new RooRealVar("timeBck_tauPos1", "Bck Tau Pos 1", 1.40183, 0.05, 2.0);
    RooRealVar *timeBck_tauPos2      = new RooRealVar("timeBck_tauPos2", "Bck Tau Pos 2", 1.84001e-01, 0.05, 1.0);
    RooRealVar *timeBck_tauNeg       = new RooRealVar("timeBck_tauNeg",  "Bck Tau Neg",   1.10029e-01, 1e-4, 1.0);
    //RooRealVar *timeBck_tauNeg2      = new RooRealVar("timeBck_tauNeg2",  "Bck Tau Neg2", 2.10029e-01, 1e-4, 1.0);//2.14564e-01
    RooRealVar *timeBck_deltatauNeg      = new RooRealVar("timeBck_deltatauNeg",  "Bck Tau Neg2", 5.10029e-02, 1e-8, 0.2);//2.14564e-01
    RooFormulaVar *timeBck_tauNeg2 = new RooFormulaVar("timeBck_tauNeg2", "timeBck_tauNeg2" , "@0+@1", RooArgList(*timeBck_tauNeg, *timeBck_deltatauNeg)); 
    
    RooRealVar *timeBck_fractionPos1 = new RooRealVar("timeBck_fractionPos1", "Fraction of PositiveExpo1",8.27597e-02, 0., 1.);
    RooRealVar *timeBck_fractionPos2 = new RooRealVar("timeBck_fractionPos2", "Fraction of PositiveExpo2",2.30404e-01, 0., 1.);
    RooRealVar *timeBck_fractionNeg  = new RooRealVar("timeBck_fractionNeg",  "Fraction of NegativeExpo",2.61591e-02,  0., 1.);
    RooRealVar *timeBck_fractionNeg2 = new RooRealVar("timeBck_fractionNeg2",  "Fraction of NegativeExpo2",2.61591e-02,  0., 1.);//3.89775e-03
    RooDecay *timeBck_decayPos1      = new RooDecay  ("timeBck_decayPos1", "Decay Pos 1", *time, *timeBck_tauPos1, *timeResolutionPDF, RooDecay::SingleSided);
    RooDecay *timeBck_decayPos2      = new RooDecay  ("timeBck_decayPos2", "Decay Pos 2", *time, *timeBck_tauPos2, *timeResolutionPDF, RooDecay::SingleSided);
    RooDecay *timeBck_decayNeg       = new RooDecay  ("timeBck_decayNeg",  "Decay Neg",   *time, *timeBck_tauNeg,  *timeResolutionPDF, RooDecay::Flipped);
    RooDecay *timeBck_decayNeg2      = new RooDecay  ("timeBck_decayNeg2",  "Decay Neg2",   *time, *timeBck_tauNeg2,  *timeResolutionPDF, RooDecay::Flipped);
    RooAddPdf* timeBckPDF      = new RooAddPdf ("timeBck_total", "Lifetime Background (Prompt+2NegativeExpo+2PositiveExpo)", RooArgList(*timeBck_decayPos1, *timeBck_decayPos2, *timeBck_decayNeg, *timeBck_decayNeg2, *timeResolutionPDF), RooArgList(*timeBck_fractionPos1, *timeBck_fractionPos2, *timeBck_fractionNeg, *timeBck_fractionNeg2), kTRUE);

    RooProdPdf* timeResolutionPDF_cond = new RooProdPdf("timeResolutionPDF_cond","timeBck_decayPos1_cond",*backgroundPunziPDF,Conditional(*timeResolutionPDF,*time)) ;
    RooProdPdf* timeBck_decayPos1_cond = new RooProdPdf("timeBck_decayPos1_cond","timeBck_decayPos1_cond",*backgroundPunziPDF,Conditional(*timeBck_decayPos1,*time)) ;
    RooProdPdf* timeBck_decayPos2_cond = new RooProdPdf("timeBck_decayPos2_cond","timeBck_decayPos1_cond",*backgroundPunziPDF,Conditional(*timeBck_decayPos2,*time)) ;
    RooProdPdf* timeBck_decayNeg_cond = new RooProdPdf("timeBck_decayNeg_cond","timeBck_decayPos1_cond" ,*backgroundPunziPDF,Conditional(*timeBck_decayNeg,*time)) ;
    RooProdPdf* timeBck_decayNeg2_cond = new RooProdPdf("timeBck_decayNeg2_cond","timeBck_decayPos1_cond",*backgroundPunziPDF,Conditional(*timeBck_decayNeg2,*time)) ;
    RooAddPdf* timeBckPDFCond      = new RooAddPdf ("timeBckPDFCond", "Lifetime Background (Prompt+2NegativeExpo+2PositiveExpo)", RooArgList(*timeBck_decayPos1_cond, *timeBck_decayPos2_cond, *timeBck_decayNeg_cond, *timeBck_decayNeg2_cond, *timeResolutionPDF_cond), RooArgList(*timeBck_fractionPos1, *timeBck_fractionPos2, *timeBck_fractionNeg, *timeBck_fractionNeg2), kTRUE);
    












    

    RooProdPdf *backgroundPDF;
    if(useMassPCE){
        backgroundPDF = new RooProdPdf("backgroundPDF","backgroundPDF",RooArgSet(*massBackgroundPDFCond,*timeBckPDFCond));
    }else{
        backgroundPDF = new RooProdPdf("backgroundPDF","backgroundPDF",RooArgSet(*massBckPDF,*timeBckPDFCond));
    }
    //Total
    RooRealVar *sigFrac  = new RooRealVar ("sigFrac", "sigFrac",mass_sigFrac->getValV(), 0., 1.);
    RooAddPdf* totPDF = new RooAddPdf  ("totPDF", "totPDF", RooArgList(*signalPDF, *backgroundPDF), *sigFrac, kTRUE);

    
    
    
    
        sigFrac               ->setVal(4.7106e-01);
    timeBck_deltatauNeg   ->setVal(3.1927e-08);
    timeBck_fractionNeg   ->setVal(1.7435e-02);
    timeBck_fractionNeg2  ->setVal(1.5690e-02);
    timeBck_fractionPos1  ->setVal(3.0896e-01);
    timeBck_fractionPos2  ->setVal(2.4781e-01);
    timeBck_tauNeg        ->setVal(1.0582e-01);
    timeBck_tauPos1       ->setVal(1.4589e+00);
    timeBck_tauPos2       ->setVal(2.3385e-01);
    timeResolution_SF     ->setVal(1.0697e+00);
    timeSignal_tau        ->setVal(1.5169e+00);
    //Total fit
    if(useWeight)result = totPDF->fitTo(*wdata,Save(),NumCPU(nCPU),SumW2Error(kTRUE));
    else result = totPDF->fitTo(*wdata,Save(),NumCPU(nCPU));

    ///////////////likelihood scan
    RooAbsReal* nll = totPDF->createNLL(*data,NumCPU(nCPU)) ;
    timeSignal_tau->setRange(1.506,1.518);
    RooPlot *nllframe = timeSignal_tau->frame(RooFit::Bins(5));
    RooAbsReal *nllprof = nll->createProfile(*timeSignal_tau);
    nllprof->plotOn(nllframe,RooFit::ShiftToZero (), RooFit::Precision(-1),Normalization(2));
    TCanvas *cNLL = new TCanvas("cnll","",1200,900);
    cNLL->cd();
    gPad->SetLeftMargin(0.15);
    nllframe->GetYaxis()->SetTitle("-2 #Delta log(L)");
    nllframe->GetYaxis()->SetTitleOffset(1.6);
    nllframe->GetYaxis()->SetRangeUser(0,nllframe->GetMaximum());
    
    nllframe->Draw();
    TLine * lin = new TLine(timeSignal_tau->getVal(),0, timeSignal_tau->getVal(),nllframe->GetMaximum());
    lin -> SetLineColor(2);
    lin -> SetLineColor(2);
    lin  ->Draw();
    cNLL->SaveAs(tag+"_nllprof.pdf");
    
    
    if(!toyMC){
        workspace->import(*totPDF); 
        workspace->import(*wdata);
        workspace->Print();
        workspace->writeToFile(tag+"BdMassLifetimeFitResults.root", kTRUE); 
    }
    //Plot the data to the figure
    //DrawPunziPartofPDF
    TCanvas *c = new TCanvas("c","",3*1200,3*900);
    TPad* pads[12];
    for(int i = 0;i<3;i++){
        pads[i]   = new TPad(Form("pad%d",i),   "",i*0.3,1.0,0.3*(i+1),0.7);
        pads[3+i] = new TPad(Form("pad%d",3+i), "",i*0.3,0.7,0.3*(i+1),0.4);
        pads[6+i] = new TPad(Form("pad%d",6+i), "",i*0.3,0.4,0.3*(i+1),0.4*0.2);
        pads[9+i] = new TPad(Form("pad%d",9+i), "",i*0.3,0.4*0.2,0.3*(i+1),0.0);
    }
    for(int i = 0;i<12;i++)pads[i]->Draw();

    
    RooPlot* simpleMassFrame    = (RooPlot*)mass->frame();
    RooPlot* timeErrFrame = (RooPlot*)timeErr->frame();
    RooPlot* massErrFrame = (RooPlot*)massErr->frame();
    RooPlot* ptFrame = (RooPlot*)pt->frame();


    wdata->plotOn(simpleMassFrame,DataError(RooAbsData::SumW2));
    simpleMassPDF->plotOn(simpleMassFrame,RooFit::Components("massSignalPDF"),LineColor(2));
    simpleMassPDF->plotOn(simpleMassFrame,RooFit::Components("massBckPDF"),LineColor(1));
    simpleMassPDF->plotOn(simpleMassFrame);
    wdata->plotOn(timeErrFrame,DataError(RooAbsData::SumW2));
    totPunziPdf->plotOn(timeErrFrame);
    totPunziPdf->plotOn(timeErrFrame,RooFit::Components("signalPunziPDF"),LineColor(2));
    totPunziPdf->plotOn(timeErrFrame,RooFit::Components("backgroundPunziPDF"),LineColor(1));
    wdata->plotOn(massErrFrame,DataError(RooAbsData::SumW2));
    totPunziPdfMassErr->plotOn(massErrFrame);
    totPunziPdfMassErr->plotOn(massErrFrame,RooFit::Components("signalPunziPDFMassErr"),LineColor(2));
    totPunziPdfMassErr->plotOn(massErrFrame,RooFit::Components("backgroundPunziPDFMassErr"),LineColor(1));
    wdata->plotOn(ptFrame,DataError(RooAbsData::SumW2));
    totPunziPdfPt->plotOn(ptFrame);
    totPunziPdfPt->plotOn(ptFrame,RooFit::Components("signalPunziPDFPt"),LineColor(2));
    totPunziPdfPt->plotOn(ptFrame,RooFit::Components("backgroundPunziPDFPt"),LineColor(1));
    
    pads[0]->cd();
    simpleMassFrame->Draw();
    pads[1]->cd();
    timeErrFrame->Draw();
    pads[3]->cd();
    massErrFrame->Draw();
    pads[4]->cd();
    ptFrame->Draw();

    RooPlot* massFrame    = (RooPlot*)mass->frame();
    RooPlot* timeFrame = (RooPlot*)time->frame();
    time->setRange("zoom",-0.2,0.2);
    RooPlot* timeZoomFrame = (RooPlot*)time->frame(Range("zoom"));

    wdata->plotOn(massFrame,DataError(RooAbsData::SumW2));
    totPDF->plotOn(massFrame,RooFit::Components("signalPDF"),LineColor(2));
    totPDF->plotOn(massFrame,RooFit::Components("backgroundPDF"),LineColor(1));
    totPDF->plotOn(massFrame);
    wdata->plotOn(timeFrame,DataError(RooAbsData::SumW2));
    totPDF->plotOn(timeFrame,RooFit::Components("signalPDF"),LineColor(2));
    totPDF->plotOn(timeFrame,RooFit::Components("backgroundPDF"),LineColor(1));
    totPDF->plotOn(timeFrame);
    wdata->plotOn(timeZoomFrame,DataError(RooAbsData::SumW2));
    totPDF->plotOn(timeZoomFrame,RooFit::Components("signalPDF"),LineColor(2));
    totPDF->plotOn(timeZoomFrame,RooFit::Components("backgroundPDF"),LineColor(1));
    totPDF->plotOn(timeZoomFrame);

    RooHist* hpullMass = massFrame->pullHist() ;
    RooPlot* massFramePull = (RooPlot*)mass->frame() ;
    massFramePull->addPlotable(hpullMass,"P") ;

    RooHist* hpullTime = timeFrame->pullHist() ;
    RooPlot* timeFramePull = (RooPlot*)time->frame() ;
    timeFramePull->addPlotable(hpullTime,"P") ;

    RooHist* hpullTimeZoom = timeZoomFrame->pullHist() ;
    RooPlot* timeZoomFramePull = (RooPlot*)time->frame(Range("zoom")) ;
    timeZoomFramePull->addPlotable(hpullTimeZoom,"P") ;

    pads[6]->cd();
    massFrame->Draw();
    pads[7]->cd();
    pads[7]->SetLogy();
    timeFrame->SetMinimum(0.001);
    timeFrame->Draw();
    pads[8]->cd();
    pads[8]->SetLogy();
    timeZoomFrame->SetMinimum(0.001);
    timeZoomFrame->Draw();
    pads[9]->cd();
    massFramePull->Draw();
    pads[10]->cd();
    timeFramePull->Draw();
    pads[11]->cd();
    timeZoomFramePull->Draw();

    
    sgDescription += "_"+bgDescription+tag;
    result->Print("v");
    cout << "Total number of events:" <<wdata->numEntries() <<endl;
    if(years.find(year) != years.end())c->SaveAs(Form("../run/BdMassLifetime%s.png",year.Data()));
    else if(year == "2016") c->SaveAs("../run/BdMassLifetime2016.png");
    else c->SaveAs("../BdMassLifetimeFullRun2_"+sgDescription+"2.pdf");
    
        std::ofstream outfile;
    //const TMatrixDSym &cor = result->correlationMatrix();
    const TMatrixDSym &cov = result->covarianceMatrix();
    Double_t x,y;
    Int_t counterp=0;
    Int_t countern=0;
    Double_t maxcheck=0,mincheck=0;
//     Double_t meanCheck=0;
    /*for (int i=0; i<hpullSimpleMass->GetN();++i){
    
        hpullSimpleMass->GetPoint(i,x,y);
//         cout<<x<<" "<<y<<endl;
        if (y>3) counterp++;
        else if (y<-3) countern++;
        if (y>maxcheck)      maxcheck=y;
        else if (y<mincheck) mincheck=y;
        
//         meanCheck+=y;
    }*/
    outfile.open("./BdMassLifetimeFullRun2_"+sgDescription+".txt", std::ios_base::out); // append instead of overwrite
    /*outfile << "chi2/ndf: "<<Form("%.3f",chi)<<endl;//", pullsOff+-3: "<<counterp+countern<<"; (>+3)"<<counterp<<" (<-3)"<<countern<<endl;
    outfile << "pullsOff+-3: "<<counterp+countern<<" largerThan+3: "<<counterp<<"smallerThan-3: "<<countern<<endl;
    outfile <<"pullMin: "<< Form("%.2f",mincheck)<<", pullMax: "<<Form("%.2f",maxcheck)<<endl; 
    outfile <<"pullMean: "<< Form("%.3f",hpullSimpleMass->GetMean(2))<<", pullRMS: "<<Form("%.3f",hpullSimpleMass->GetRMS(2))<<endl; 
    */result->printMultiline(outfile,100,kTRUE);
    outfile.close();
    
    setGoodMiddleScaleSymmetric();
    gStyle->SetPaintTextFormat("1.3f");
    TCanvas *cCor = new TCanvas("cCor","",1200,900);
//     for(int i=0; i<simpleMassFramePull->GetNbins();++i) cout<<simpleMassFramePull->GetBinContent(i)<<endl;
    TH2 *cor = result->correlationHist();
    cCor->cd();
    cor->Draw("text colz");
    gStyle->SetOptStat(0);
    cCor->SaveAs("./BdMassLifetimeFullRun2_"+sgDescription+"_cor.pdf");	

}
