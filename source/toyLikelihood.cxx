 
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "TCanvas.h"
#include "RooPlot.h"
#include "TAxis.h"
using namespace RooFit;
 
void toyLikelihood()
{
   // S e t u p   m o d e l
   // ---------------------
 
   // Declare variables x,mean,sigma with associated name, title, initial value and allowed range
   RooRealVar x("x", "x", -10, 10);
   RooRealVar mean("mean", "mean of gaussian", 1, -10, 10);
   RooRealVar sigma("sigma", "width of gaussian", 1, 0.1, 10);
 
   // Build gaussian pdf in terms of x,mean and sigma
   RooGaussian gauss("gauss", "gaussian PDF", x, mean, sigma);
 
   // Construct plot frame in 'x'
   RooPlot *xframe = x.frame(Title("Gaussian pdf."));
 
   // P l o t   m o d e l   a n d   c h a n g e   p a r a m e t e r   v a l u e s
   // ---------------------------------------------------------------------------
 
   // Plot gauss in frame (i.e. in x)
   gauss.plotOn(xframe);
 
   // Change the value of sigma to 3
   sigma.setVal(3);
 
   // Plot gauss in frame (i.e. in x) and draw frame on canvas
   gauss.plotOn(xframe, LineColor(kRed));
 
   // G e n e r a t e   e v e n t s
   // -----------------------------
 
   // Generate a dataset of 1000 events in x from gauss
   RooDataSet *data = gauss.generate(x, 1000000);
 
   // Make a second plot frame in x and draw both the
   // data and the pdf in the frame
   RooPlot *xframe2 = x.frame(Title("Gaussian pdf with data"));
   data->plotOn(xframe2);
   gauss.plotOn(xframe2);
 
   // F i t   m o d e l   t o   d a t a
   // -----------------------------
// mean.setConstant(kTRUE);
//  mean.setVal(3);
   // Fit pdf to data
   RooFitResult * result = gauss.fitTo(*data,NumCPU(8),Save());
   RooAbsReal* nll = gauss.createNLL(*data,NumCPU(8)) ;
//    RooMinuit m(*nll) ;
       mean.setRange(mean.getVal()-0.1,mean.getVal()+0.1);
    RooAbsReal *nprof = nll->createProfile(mean);
    RooPlot *nllframe = mean.frame(RooFit::Bins(10));
    nprof->plotOn(nllframe,RooFit::ShiftToZero (), RooFit::Precision(-1));
   
    RooMinuit(*nll).migrad();
   RooAbsReal *nprof2 = nll->createProfile(mean);
        RooPlot *nllframe2 = mean.frame();
//      nprof2->plotOn(nllframe2,RooFit::ShiftToZero (),Normalization(2));
    
   // Print values of mean and sigma (that now reflect fitted values and errors)
   mean.Print();
   sigma.Print();
 
   // Draw all frames on a canvas
   TCanvas *c = new TCanvas("rf101_basics", "rf101_basics", 800, 400);
   c->Divide(3);
   c->cd(1);
//    gPad->SetLeftMargin(0.15);
//    xframe->GetYaxis()->SetTitleOffset(1.6);
//    xframe->Draw();
//    c->cd(2);
   gPad->SetLeftMargin(0.15);
   xframe2->GetYaxis()->SetTitleOffset(1.6);
   xframe2->Draw();
   c->cd(2);
   gPad->SetLeftMargin(0.15);
   nllframe->GetYaxis()->SetTitleOffset(1.6);
   nllframe->Draw();
   c->cd(3);
   gPad->SetLeftMargin(0.15);
   nllframe2->GetYaxis()->SetTitleOffset(1.6);
   nllframe2->GetYaxis()->SetTitle("-2 #Delta log(L)");
   nllframe2->GetYaxis()->SetRangeUser(0,nllframe2->GetMaximum());
   nllframe2->Draw();
    TLine * lin = new TLine(mean.getVal(),0, mean.getVal(),nllframe2->GetMaximum());
    lin -> SetLineColor(2);
    lin -> SetLineColor(2);
    lin  ->Draw();
   c->SaveAs("toyLikelihood.pdf");
   result->Print("v");
   
}
