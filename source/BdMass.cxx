 #include "RooRealVar.h"
 #include "RooDataSet.h"
 #include "RooDataHist.h"
 #include "RooGaussian.h"
 #include "RooGaussModel.h"
 #include "RooExponential.h"
 #include "TCanvas.h"
 #include "RooPlot.h"
 #include "TTree.h"
 #include "TFile.h"
 #include "TH1D.h"
 #include "TRandom.h"
 #include "RooAddPdf.h"
 #include "RooDecay.h"
 #include "RooProdPdf.h"
 #include "TH1F.h"
 #include "TH2F.h"
 #include "RooHistPdf.h"
 #include "RooFormulaVar.h"
 #include "RooProduct.h"
 #include "TRandom3.h"
 #include "RooHist.h"
 #include "RooFitResult.h"
 #include "RooCategory.h"


 #include "RooPDFs/RooJpsiXPdf.h"
 #include "RooPDFs/RooJpsiXPdf.cxx"
 
 #include "RooPDFs/RooCrystalBallnewgeneral.h"
 #include "RooPDFs/RooCrystalBallnewgeneral.cxx"
 #include "RooJohnson.h"

using namespace RooFit;

    Double_t massMin = 5000;
    Double_t massMax = 5650;

    Double_t tauMin = -2;
    Double_t tauMax = 20;

    Double_t tauErrMin = .018;
    Double_t tauErrMax = .12;

    Double_t massErrMin = 10;
    Double_t massErrMax = 100;

    Double_t ptMin = 10;
    Double_t ptMax = 100;

    set<TString> years {"2015", "2016","2016A", "2016B", "2016C", "2017", "2018"};

void setGoodMiddleScale(){
	// Taken from http://ultrahigh.org/2007/08/20/making-pretty-root-color-palettes/
  const Int_t NRGBs = 9;
  const Int_t NCont = 255;
  Double_t stops[NRGBs] = {   0.00, 0.20, 0.35, 0.50, 0.65, 0.80, 0.90, 0.99, 1.0 };
  Double_t red  [NRGBs] = {   0.30, 0.20, 0.10, 0.00, 1.0, 1.00, 1.00, 1.00, 0. };
  Double_t green[NRGBs] = {   0.10, 0.20, 1.0, 1.00, 1.0, 0.80, 0.40, 0.00, 1. };
  Double_t blue [NRGBs] = {   0.40, 1.00, 1.0, 0.0, 0.00, 0.00, 0.00, 0.00, 0. };
  TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
  gStyle->SetNumberContours(NCont);
}

void setGoodMiddleScaleSymmetric(){
	// Taken from http://ultrahigh.org/2007/08/20/making-pretty-root-color-palettes/
  const Int_t NRGBs = 11;
  const Int_t NCont = 255;
  Double_t stops[NRGBs] = {   0.00, 0.01, 0.10, 0.25, 0.40, 0.50, 0.60, 0.75, 0.90, 0.99, 1.00 };
  Double_t red  [NRGBs] = {   1.00, 1.00, 1.00, 1.00, 0.50, 0.00, 0.50, 1.00, 1.00, 1.00, 1.00 };
  Double_t green[NRGBs] = {   0.00, 0.10, 0.50, 1.00, 1.00, 1.00, 1.00, 1.00, 0.50, 0.10, 0.00 };
  Double_t blue [NRGBs] = {   0.00, 0.00, 0.00, 0.00, 0.50, 0.00, 0.50, 0.00, 0.00, 0.00, 0.00 };
  TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
  gStyle->SetNumberContours(NCont);
}
    
TLegend *drawAtlasLegend(Double_t x1, Double_t x2, Double_t legendTop, Double_t legendSize, Int_t nSteps, Double_t margin = 0.15, Double_t scale = 1.0) {
    
    TLegend *l = new TLegend(x1, legendTop-(nSteps-1)*legendSize*1.36*scale, x2, legendTop+legendSize*0.9);
    l->SetBorderSize(0);
    l->SetFillStyle(0);
    l->SetMargin(margin);
    l->SetTextSize(legendSize);
    l->SetTextFont(42);
    
    return l;
}

void BdMass(TString sgDescription = "dg", TString bgDescription = "expJpsiX", TString tag="", bool useMassPCE = false,
            TString year = "",bool useWeight = false,int sample = -1){
    RooCategory* yearsCategory = new RooCategory("yearsCategory","yearsCategory");
    yearsCategory->defineType("2015" ) ;
    yearsCategory->defineType("2016A") ;
    yearsCategory->defineType("2016B") ;
    yearsCategory->defineType("2016C") ;
    yearsCategory->defineType("2017" ) ;
    yearsCategory->defineType("2018" ) ;
    
    bool useTriggers = true;
    

    int nBins = 100;
    int nCPU = 8;
    TLegend* l = drawAtlasLegend(0.60, 0.95, 0.85, 0.035, 2); 
    TLegend* l2 = drawAtlasLegend(0.60, 0.95, 0.85, 0.035, 2); 
    //READ the data from file and put them to the RooDataset

    RooFitResult *result;
    RooFitResult *resultTot;
    RooRealVar *mass = new RooRealVar("B_mass", "B_mass", massMin, massMax);
    RooRealVar *time = new RooRealVar("B_tau_MinA0", "B_tau_MinA0", tauMin, tauMax);
    RooRealVar *massErr = new RooRealVar("B_mass_err", "B_mass_err", massErrMin, massErrMax);
    RooRealVar *timeErr = new RooRealVar("B_tau_MinA0_err", "B_tau_MinA0_err", tauErrMin, tauErrMax);
    RooRealVar *pt = new RooRealVar("B_pT", "B_pT", ptMin, ptMax);
    RooArgSet*  vars = new RooArgSet(*mass, *time, *massErr, *timeErr, *pt,*yearsCategory);

    RooDataSet* wdata = NULL;    
    RooDataSet* dataTmp = NULL;    
    RooDataSet* data = NULL;    

        TFile* f = new TFile("../data/RooDatasetBd.root","READ") ;
        dataTmp = (RooDataSet*)f->Get("data"); 
        f->Close();

    if(years.find(year) != years.end()){
        data = (RooDataSet*) dataTmp->reduce(*vars,Form("yearsCategory==yearsCategory::%s",year.Data())) ;
    }
    else{
        data=dataTmp;
    }    
    data->Print();

    if(useWeight){
        wdata = new RooDataSet(data->GetName(),data->GetTitle(),data,*data->get(),0,"w") ;
    }
    else wdata = data;

    if (sample > 0) {
    Long64_t nOrig = wdata->numEntries();
    TString title = wdata->GetTitle();
    title += " Random sample ( ";
    title += sample;
    title += " / ";
    title += nOrig;
    title += " )";
    RooDataSet *newDataset = new RooDataSet("wdataRandomSample", title, *vars);
    TRandom3 rand(0);
    for ( Long64_t i = 0; i < sample; i++ ) {
        Long64_t j = rand.Uniform( 0 , nOrig );
        newDataset->add( *(wdata->get( j )) );
    }
    cout << "INFO: Created random sample (" << sample << " from " << nOrig << ")" << endl;
    wdata=newDataset;

    }
    
    ///Prepare model
    ////Signal
    //Mass
    TString niceName;
    
    int nParamsSimple = 0;
    int nParamsTot = 0;
    RooAbsPdf* massSignalPDF;
    RooRealVar *mass_mean      = new RooRealVar ("massSignal_mean", "Mean of Gaussian", 5279,5200, 5350);
    cout<<sgDescription<<endl;
    

    RooArgSet *params = new RooArgSet("params");
    RooArgSet *sgParams = new RooArgSet("sgParams");
    RooRealVar *massSignal_mean      = new RooRealVar ("massSignal_mean", "Mean1", 5279,5200, 5350);
    RooRealVar *massSignal_fraction  = new RooRealVar ("massSignal_fraction", "Fraction of Double",0.9,    0.8,    0.999);
    //jj
    RooRealVar *massSignal_gamma  = new RooRealVar  ("massSignal_gamma", "", 3,-10,10);
    RooRealVar *massSignal_gamma2  = new RooRealVar ("massSignal_gamma2", "", 3,-10,10);
    RooRealVar *massSignal_delta  = new RooRealVar  ("massSignal_delta", "", 4,0,10);
    RooRealVar *massSignal_delta2  = new RooRealVar ("massSignal_delta2", "", 4,0,10);
    RooRealVar *massSignal_lambda  = new RooRealVar ("massSignal_lambda", "", 50,0.0001,5000);
    RooRealVar *massSignal_lambda2  = new RooRealVar("massSignal_lambda2", "", 50,0.0001,5000);
    RooRealVar *massSignal_mean2 = new RooRealVar   ("massSignal_mean2", "", 5280,5250,5320);
    //dgfm
    RooRealVar *massSignal_sigma1    = new RooRealVar ("massSignal_sigma1", "Sigma1 of DoubleGaussian", 20, 1, 30);
    RooRealVar *massSignal_sigma2    = new RooRealVar ("massSignal_sigma2", "Sigma2 of DoubleGaussian",40, 1, 70);

    
    if (sgDescription == "cbpe2"){
        RooRealVar *alfaL  = new RooRealVar ("alfaL", "", 1.61509,0.5,8);
        RooRealVar *alfaR  = new RooRealVar ("alfaR", "", 1.94879,0.5,8);
        RooRealVar *nL     = new RooRealVar ("nL", "",  20,1,200);
        RooRealVar *nR     = new RooRealVar ("nR", "",  20,1,200);
        
        RooRealVar *s1L     = new RooRealVar ("s1L", "",   1.25567e+00,0.8,1.35);
        RooRealVar *s2L     = new RooRealVar ("s2L", "",  0.,-0.0025,0.05);
        //general only
        RooRealVar *s1R     = new RooRealVar ("s1R", "",   1.34821e+00,0.8,1.35);
        RooRealVar *s2R     = new RooRealVar ("s2R", "",  0.,-0.0025,0.05);

        RooRealVar *s0L     = new RooRealVar ("s0L", "",  0., -0.02,0.02);
        RooRealVar *s0R     = new RooRealVar ("s0R", "",  0., -0.02,0.02);
        
        s0L->setConstant(kTRUE);
        s0R->setConstant(kTRUE);
        
//         nL->setConstant(kTRUE);
//         nR->setConstant(kTRUE);
//         sgDescription+="fixedN20";
//         nParamsSimple -=2;
        
        massSignalPDF = new RooCrystalBall("massSignalPDF","",*mass,*massSignal_mean,*massErr,*alfaL,*nL,*alfaR,*nR,*s1L,*s2L, *s1R, *s2R,*s0L,*s0R);  
        nParamsSimple += 9;
        niceName = "Crystal Ball with Quadratic Per-Candidate Error";

    }    if (sgDescription == "cbpe1"){
        RooRealVar *alfaL  = new RooRealVar ("alfaL", "", 1.61509,0.5,8);
        RooRealVar *alfaR  = new RooRealVar ("alfaR", "", 1.94879,0.5,8);
        RooRealVar *nL     = new RooRealVar ("nL", "",  20,1,200);
        RooRealVar *nR     = new RooRealVar ("nR", "",  20,1,200);
        
        RooRealVar *s1L     = new RooRealVar ("s1L", "",   1.25567e+00,0.8,1.35);
        RooRealVar *s2L     = new RooRealVar ("s2L", "",  0.,-0.0025,0.05);
        //general only
        RooRealVar *s1R     = new RooRealVar ("s1R", "",   1.34821e+00,0.8,1.35);
        RooRealVar *s2R     = new RooRealVar ("s2R", "",  0.,-0.0025,0.05);

        RooRealVar *s0L     = new RooRealVar ("s0L", "",  0., -0.02,0.02);
        RooRealVar *s0R     = new RooRealVar ("s0R", "",  0., -0.02,0.02);
        
        s0L->setConstant(kTRUE);
        s0R->setConstant(kTRUE);
        
        s2L->setConstant(kTRUE);
        s2R->setConstant(kTRUE);
        
//         nL->setConstant(kTRUE);
//         nR->setConstant(kTRUE);
//         sgDescription+="fixedN20";
//         nParamsSimple -=2;
        
        massSignalPDF = new RooCrystalBall("massSignalPDF","",*mass,*massSignal_mean,*massErr,*alfaL,*nL,*alfaR,*nR,*s1L,*s2L, *s1R, *s2R,*s0L,*s0R);  
        nParamsSimple += 7;
        niceName = "Crystal Ball with Linear Per-Candidate Error";

    }else if (sgDescription == "cb"){
        RooRealVar *massErr1     = new RooRealVar ("massErr1", "massErr1",  1.,1.,1.);
        
        RooRealVar *alfaL  = new RooRealVar ("alfaL", "", 1.61509,0.5,8);
        RooRealVar *alfaR  = new RooRealVar ("alfaR", "", 1.94879,0.5,8);
        RooRealVar *nL     = new RooRealVar ("nL", "",  10,1,200);
        RooRealVar *nR     = new RooRealVar ("nR", "",  10,1,200);
        
        RooRealVar *s1L     = new RooRealVar ("s1L", "",   1.25567e+00,0.8,1.35);
        RooRealVar *s2L     = new RooRealVar ("s2L", "",  0.,-0.0025,0.05);
        //general only
        RooRealVar *s1R     = new RooRealVar ("s1R", "",   1.34821e+00,0.8,1.35);
        RooRealVar *s2R     = new RooRealVar ("s2R", "",  0.,-0.0025,0.05);

        RooRealVar *s0L     = new RooRealVar ("s0L", "",  0., -0.02,0.02);
        RooRealVar *s0R     = new RooRealVar ("s0R", "",  0., -0.02,0.02);
        
        s1L->setRange(0.5,100);
        s1R->setRange(0.5,100);
        
        s0L->setConstant(kTRUE);
        s0R->setConstant(kTRUE);
        s2L->setConstant(kTRUE);
        s2R->setConstant(kTRUE);
        
        nL->setConstant(kTRUE);
        nR->setConstant(kTRUE);
        sgDescription+="fixedN10";
        niceName = "Crystal Ball";
        nParamsSimple -=2;
//         
        massSignalPDF = new RooCrystalBall("massSignalPDF","",*mass,*massSignal_mean,*massErr1,*alfaL,*nL,*alfaR,*nR,*s1L,*s2L, *s1R, *s2R,*s0L,*s0R);  
        nParamsSimple += 7;

    }else if(sgDescription =="dg"){
        niceName = "Double Gaussian";
        RooGaussian *mass_gauss1   = new RooGaussian("massSignal_gauss1", "Gaussian 1", *mass, *massSignal_mean, *massSignal_sigma1);
        RooGaussian *mass_gauss2   = new RooGaussian("massSignal_gauss2", "Gaussian 2", *mass, *massSignal_mean, *massSignal_sigma2);
        
        massSignalPDF = new RooAddPdf  ("massSignalPDF", "Mass Signal DoubleGaussian", RooArgList(*mass_gauss1, *mass_gauss2), *massSignal_fraction, kTRUE);
        nParamsSimple += 4; //mean, sigma1, sigma2, fraction
    }else if (sgDescription == "john"){
	niceName = "Johnson's Function";
        
        massSignalPDF = new 	RooJohnson(    "massSignalPDF", "Johnson SU", *mass, *massSignal_mean, *massSignal_lambda, *massSignal_gamma, *massSignal_delta);
        nParamsSimple += 4;
         
//         delta     ->setVal(1.1452e+00);
//         gamma     ->setVal(-1.6247e-03);
//         lambda    ->setVal(3.4259e+01);
//         mass_mean ->setVal(5.2797e+03);
        
//         delta     ->setConstant(kTRUE);
//         gamma     ->setConstant(kTRUE);
// //         lambda    ->setConstant(kTRUE);
// //         mass_mean ->setConstant(kTRUE);
//         nParamsSimple -= 3;
//         sgDescription+="_fixed-1MCfreeLambdaMean";
        
        
    }else if (sgDescription == "jj" || sgDescription == "jjfm"){
        niceName = "Double Johnson's Function";
        
        massSignal_delta     ->setVal(1.1452e+00);
        massSignal_gamma     ->setVal(-1.6247e-03);
        massSignal_lambda    ->setVal(3.4259e+01);
        massSignal_mean ->setVal(5.2797e+03);
        
        nParamsSimple += 4;
        nParamsSimple += 4;
        sgParams->add(RooArgSet(*massSignal_mean,*massSignal_delta,*massSignal_gamma,*massSignal_lambda,*massSignal_delta2,*massSignal_gamma2,*massSignal_lambda2));        
        RooJohnson *jSig = new 	RooJohnson(    "JSig", "Johnson Signal Decription", *mass, *massSignal_mean, *massSignal_lambda, *massSignal_gamma, *massSignal_delta);
        if (sgDescription == "jjfm") {
            niceName += "Fixed Mean";
            massSignal_mean2 = massSignal_mean;
            nParamsSimple -= 1;
        
        } else sgParams->add(*massSignal_mean2);
        
        RooJohnson *jTune = new 	RooJohnson(    "JTune", "Johnson Tunning", *mass, *massSignal_mean2, *massSignal_lambda2, *massSignal_gamma2, *massSignal_delta2);
        
        massSignalPDF = new RooAddPdf("massSignalPDF","jj",RooArgList(*jSig,*jTune),*massSignal_fraction);
        nParamsSimple += 1;
         
    }    else {
        cout<<"ERROR: invalid function name"<<endl;
        exit(EXIT_FAILURE);
    }
    nParamsTot=nParamsSimple;
    params->add(*sgParams);
    ////Background
    //Mass
    RooArgSet *bgParams = new RooArgSet("bgParams");
    RooRealVar *simpleMassBck_slope1     = new RooRealVar    ("simpleMassBck_slope1", "Slope of Exponential1",-1.83178e-03, -0.1, -1e-8);
    RooRealVar *simpleMassBck_slope2     = new RooRealVar    ("simpleMassBck_slope2", "Slope of Exponential2",-9.83178e-04, -0.1, -1e-8);
    RooExponential *simpleMass_exponential1 = new RooExponential("simpleMass_exponential1", "mass_exponential1", *mass, *simpleMassBck_slope1);
    RooExponential *simpleMass_exponential2 = new RooExponential("simpleMass_exponential2", "mass_exponential2", *mass, *simpleMassBck_slope2);
    RooRealVar *simpleMassBck_fraction  = new RooRealVar ("simpleMassBck_fraction", "Fraction of DoubleGaussian",0.5, 0.01, 0.99);
    RooRealVar *simpleMassBck_fraction2  = new RooRealVar ("simpleMassBck_fraction2", "Fraction of DoubleGaussian",0.5, 0.01, 0.99);

    RooRealVar *simpleMassBck_scale     = new RooRealVar    ("simpleMassBck_scale", "simpleMassBck_scale",10,1, 20);
    RooRealVar *simpleMassBck_offset     = new RooRealVar    ("simpleMassBck_offset", "simpleMassBck_offset",4750, 4500, 5000);
    RooJpsiXPdf *simpleMass_JpsiX = new RooJpsiXPdf("simpleMass_JpsiX","simpleMass_JpsiX",*mass,*simpleMassBck_offset,*simpleMassBck_scale);
    
    RooRealVar *simpleMass_a1     = new RooRealVar    ("simpleMass_a1Pol" , "Linear Term of Pol",-5.0 , -10000., 10000.);
    
//     RooArgList()//const
//     RooArgList(simpleMass_a1)//lin
    RooPolynomial* simpleMass_pol = new RooPolynomial("simpleMass_pol","simpleMass_pol",*mass,RooArgList(*simpleMass_a1)); 
    RooPolynomial* simpleMass_const = new RooPolynomial("simpleMass_pol","simpleMass_pol",*mass,RooArgList()); 
    
    RooAbsPdf* simpleMassBckPDF;
    if (bgDescription == "exp"){
    //exp 
        simpleMassBckPDF = new RooExponential("simpleMassBckPDF", "mass_exponential1", *mass, *simpleMassBck_slope1);
        nParamsSimple+=1;
        bgParams->add(RooArgSet(*simpleMassBck_slope1));        
        
    }else if (bgDescription == "expexp"){
    //exp + exp
        simpleMassBckPDF = new RooAddPdf  ("simpleMassBckPDF", "Mass Background DoubleGaussian", RooArgList(*simpleMass_exponential1, *simpleMass_exponential2),*simpleMassBck_fraction);
        nParamsSimple+=3;
        bgParams->add(RooArgSet(*simpleMassBck_slope1,*simpleMassBck_slope2,*simpleMassBck_fraction));        
        
    }else if (bgDescription == "expJpsiX"){
    //exp + tanh
        simpleMassBckPDF = new RooAddPdf  ("simpleMassBckPDF", "Mass Background DoubleGaussian", RooArgList(*simpleMass_exponential1, *simpleMass_JpsiX),*simpleMassBck_fraction);
        nParamsSimple+=4; //slope1,bgFrac,scale,offset
        bgParams->add(RooArgSet(*simpleMassBck_slope1,*simpleMassBck_offset,*simpleMassBck_scale,*simpleMassBck_fraction)); 
    
    }else if (bgDescription == "polexp"){
    //exp+pol
        simpleMassBckPDF = new RooAddPdf  ("simpleMassBckPDF", "Mass Background DoubleGaussian", RooArgList(*simpleMass_pol, *simpleMass_exponential1),*simpleMassBck_fraction);
        nParamsSimple+=3; //slope1,bgFrac,a1
    
    }else if (bgDescription == "polJpsiX"){
    //tanh+pol
        simpleMassBckPDF = new RooAddPdf  ("simpleMassBckPDF", "Mass Background DoubleGaussian", RooArgList(*simpleMass_pol, *simpleMass_JpsiX),*simpleMassBck_fraction);
        nParamsSimple+=4; //a1,bgFrac,scale,offset
    }else if (bgDescription == "pol0exp"){
    //exp+pol0
        simpleMassBckPDF = new RooAddPdf  ("simpleMassBckPDF", "Mass Background DoubleGaussian", RooArgList(*simpleMass_const, *simpleMass_exponential1),*simpleMassBck_fraction);
        nParamsSimple+=2; //slope1,bgFrac
        bgParams->add(RooArgSet(*simpleMassBck_slope1,*simpleMassBck_fraction)); 
    
    }else if (bgDescription == "pol0JpsiX"){
    //tanh+pol
        simpleMassBckPDF = new RooAddPdf  ("simpleMassBckPDF", "Mass Background DoubleGaussian", RooArgList(*simpleMass_const, *simpleMass_JpsiX),*simpleMassBck_fraction);
        nParamsSimple+=4; //a1,bgFrac,scale,offset
    }else if (bgDescription == "pol0expJpsiX"){
    //tanh+pol0+exp
        simpleMassBckPDF = new RooAddPdf  ("simpleMassBckPDF", "Mass Background DoubleGaussian", RooArgList(*simpleMass_const, *simpleMass_JpsiX, *simpleMass_exponential1),RooArgList(*simpleMassBck_fraction,*simpleMassBck_fraction2));
        nParamsSimple+=5; //bgFrac,scale,offset,frac,slope1
        bgParams->add(RooArgSet(*simpleMassBck_slope1,*simpleMassBck_offset,*simpleMassBck_scale,*simpleMassBck_fraction,*simpleMassBck_fraction2)); 

    }else if (bgDescription == "pol0expexp"){
    //const+exp+exp
        simpleMassBckPDF = new RooAddPdf  ("simpleMassBckPDF", "Mass Background DoubleGaussian", RooArgList(*simpleMass_const, *simpleMass_exponential1, *simpleMass_exponential2),RooArgList(*simpleMassBck_fraction,*simpleMassBck_fraction2));
        nParamsSimple+=4; //bgFrac,scale,scale frac
        bgParams->add(RooArgSet(*simpleMassBck_slope1,*simpleMassBck_slope2,*simpleMassBck_fraction,*simpleMassBck_fraction2));
    }else {
        cout<<"ERROR: invalid function name"<<endl;
        exit(EXIT_FAILURE);
    }
    params->add(*bgParams);
    
    //Total
    RooRealVar *mass_sigFrac  = new RooRealVar ("mass_sigFrac", "mass_sigFrac",0.3, 0., 1.);
    RooAddPdf* simpleMassPDF = new RooAddPdf  ("massPDF", "massPDF", RooArgList(*massSignalPDF, *simpleMassBckPDF), *mass_sigFrac, kTRUE);
    params->add(*mass_sigFrac);
    //fix mass_JspiX
//     simpleMassBck_offset->setVal(4.6500e+03);
//     simpleMassBck_scale->setVal( 1.0000e+01);
//     simpleMassBck_offset->setConstant(kTRUE);
//     simpleMassBck_scale->setConstant(kTRUE);
//     nParamsSimple-=2;
//     sgDescription+="_fixedJpsiX";

//     mass->setRange("right", 5276.9, 5650);
//  
//    // Fit pdf only to data in "signal" range
// //    RooFitResult *r_sig = model.fitTo(*modelData, Save(kTRUE), Range("signal"));
//     
//     simpleMassBck_fraction->setVal(1);
//     simpleMassBck_fraction->setConstant(kTRUE);
//     simpleMassPDF->fitTo(*wdata,RooFit::InitialHesse(kTRUE),RooFit::SumW2Error(kTRUE),NumCPU(nCPU),Timer(kTRUE),Save(), Range("right"));
//     
//     simpleMassBck_fraction->setConstant(kFALSE);
//     simpleMassBck_slope1->setConstant(kTRUE);
//     tag+="fixedexp52769";
//     delta     ->setConstant(kTRUE);
//     gamma     ->setConstant(kTRUE);
//     lambda    ->setConstant(kTRUE);
//     mass_mean ->setConstant(kTRUE);
    
    //standalone fit
    result = simpleMassPDF->fitTo(*wdata,RooFit::InitialHesse(kTRUE),RooFit::SumW2Error(kTRUE),NumCPU(nCPU),Timer(kTRUE),Save());//, Range("full"));
    //Sideband subtraction to prepare punzi
//      mass->setRange("full", 5000, 5650);


    RooAddPdf* totPDF;
    RooAddPdf *totPunziPdfMassErr;
    if(useMassPCE){
        cout<<"Handling Punzies"<<endl;
        mass->setRange("signal",mass_mean->getValV()-80,mass_mean->getValV()+80);
        RooAbsReal* fsigRegion_model = simpleMassPDF->createIntegral(*mass,NormSet(*mass),Range("signal")); 
        RooAbsReal* fsigRegion_sig = massSignalPDF->createIntegral(*mass,NormSet(*mass),Range("signal")); 

        Double_t fsig = mass_sigFrac->getValV()*fsigRegion_sig->getValV()/fsigRegion_model->getValV();
        
        RooDataSet* dataSideband = (RooDataSet*)wdata->reduce(Form("B_mass<%f || B_mass>%f",mass_mean->getValV()-150,mass_mean->getValV()+150));
        dataSideband->SetName("dataSideband");
        RooDataSet* dataSignalBand = (RooDataSet*)wdata->reduce(Form("B_mass>%f && B_mass<%f",mass_mean->getValV()-80,mass_mean->getValV()+80));
        dataSignalBand->SetName("dataSignalBand");

        TH1F* signal_histMassErr     = (TH1F*)dataSignalBand->createHistogram("signal_histMassErr",*massErr,Binning(nBins,massErrMin,massErrMax));
        TH1F* background_histMassErr = (TH1F*)dataSideband->createHistogram("background_histMassErr",*massErr,Binning(nBins,massErrMin,massErrMax));
        signal_histMassErr->Scale(1.0/signal_histMassErr->Integral(), "width");
        background_histMassErr->Scale(1.0/background_histMassErr->Integral(), "width");
        signal_histMassErr->Add(background_histMassErr,-1*(1-fsig));

        RooDataHist* signal_dataHistMassErr = new RooDataHist(" signal_dataHistMassErr", " signal_dataHist", *massErr, signal_histMassErr);
        RooHistPdf* signalPunziPDFMassErr = new RooHistPdf("signalPunziPDFMassErr","signalPunziPDF", *massErr, *signal_dataHistMassErr);

        RooDataHist* background_dataHistMassErr = new RooDataHist(" background_dataHistMassErr", " background_dataHist", *massErr, background_histMassErr);
        RooHistPdf* backgroundPunziPDFMassErr = new RooHistPdf("backgroundPunziPDFMassErr","backgroundPunziPDF", *massErr, *background_dataHistMassErr);

        totPunziPdfMassErr = new RooAddPdf  ("totPunziPdfMassErr", "total punzi pdf", RooArgList(*signalPunziPDFMassErr, *backgroundPunziPDFMassErr),*mass_sigFrac, kTRUE);

        ///Prepare mass-PCE
        
        //Mass Resolution
        RooRealVar *massPCE_mean     = new RooRealVar   ("massPCE_mean", "Mean (0.0) of Mass Resolution", mass_mean->getValV(),5250,5300);
        RooRealVar *massPCE_SF  = new RooRealVar   ("massPCE_SF", "SF of Mass Error", 1.28, 0.1, 10.);
        RooProduct *massPCE_sigma       = new RooProduct("massPCE_sigma","massErr * Scale Factor", RooArgList(*massErr, *massPCE_SF));
        RooGaussian *massResolutionPDF  = new RooGaussian("massResolution", "Mass Resolution Gaussian (for both Sig and Bck)", *mass, *massPCE_mean,*massPCE_sigma);
        nParamsTot+=2;

        RooProdPdf* massSignalPDFCond = new RooProdPdf("massSignalPDFCond","massSignalPDFCond",*signalPunziPDFMassErr,Conditional(*massResolutionPDF,*mass)) ;


        RooRealVar *mass_slope1     = new RooRealVar    ("massBck_slope1", "Slope of Exponential1",simpleMassBck_slope1->getValV(), -1, -1e-8);
        nParamsTot+=1;
        RooRealVar *mass_slope2     = new RooRealVar    ("massBck_slope2", "Slope of Exponential2",simpleMassBck_slope2->getValV(), -1, -1e-8);
        RooExponential *mass_exponential1 = new RooExponential("mass_exponential1", "mass_exponential1", *mass, *mass_slope1);
        RooExponential *mass_exponential2 = new RooExponential("mass_exponential2", "mass_exponential2", *mass, *mass_slope2);
        RooRealVar *massBck_fraction  = new RooRealVar ("massBck_fraction", "Fraction of DoubleGaussian",simpleMassBck_fraction->getValV());//, 0., 1.); //const

        RooRealVar *mass_scale     = new RooRealVar    ("mass_scale", "mass_scale",simpleMassBck_scale->getValV());//,1, 20);              //const
        RooRealVar *mass_offset     = new RooRealVar    ("mass_offset", "mass_offset",simpleMassBck_offset->getValV());//, 4500, 5000);    //const
        RooJpsiXPdf *mass_JspiX = new RooJpsiXPdf("mass_JspiX","mass_JspiX",*mass,*mass_offset,*mass_scale);
        
        //RooAddPdf* massBckPDF = new RooAddPdf  ("massBckPDF", "Mass Background DoubleGaussian", RooArgList(*mass_exponential1, *mass_exponential2),*massBck_fraction);
        RooAddPdf* massBckPDF = new RooAddPdf  ("massBckPDF", "Mass Background DoubleGaussian", RooArgList(*mass_exponential1, *mass_JspiX),*massBck_fraction);
        RooProdPdf* massBackgroundPDFCond = new RooProdPdf("massBackgroundPDFCond","massSignalPDFCond",*backgroundPunziPDFMassErr,Conditional(*massBckPDF,*mass)) ;

        RooRealVar *sigFrac  = new RooRealVar ("sigFrac", "sigFrac",mass_sigFrac->getValV(), 0., 1.);
        totPDF = new RooAddPdf  ("totPDF", "totPDF", RooArgList(*massSignalPDFCond, *massBackgroundPDFCond), *sigFrac, kTRUE);
        
        
        //Total fit
        if(useWeight)resultTot = totPDF->fitTo(*wdata,Save(),NumCPU(nCPU),SumW2Error(kTRUE));
        else resultTot = totPDF->fitTo(*wdata,Save(),NumCPU(nCPU));
    }
    //Plot the data to the figure
    //DrawPunziPartofPDF
    TCanvas *c;
    
    TCanvas *c1 = new TCanvas("c1","",1200,900);
    TCanvas *c2 = new TCanvas("c2","",1200,240);
    TCanvas *c3 = new TCanvas("c3","",1200,240);
    
    TPad* pads[8];
    if(useMassPCE){
        c = new TCanvas("c","",2*1200,2*900);
            for(int i = 0;i<2;i++){
                for(int j = 0;j<2;j++){
		        pads[4*i+2*j]     = new TPad(Form("pad%d",4*i+2*j)  ,   "",0.5*j,1-0.5*i,0.5*(j+1),1-0.5*i-0.4);
                        pads[4*i+2*j+1]   = new TPad(Form("pad%d",4*i+2*j+1),   "",0.5*j,1-0.5*i-0.4,0.5*(j+1),1-0.5*(i+1));
                }
            }
        for(int i = 0;i<8;i++)pads[i]->Draw();
    }
    else {
	c = new TCanvas("c","",1200,900);
        pads[0]= new TPad("pad0",   "",0,1  ,1,0.2);
	    pads[1]= new TPad("pad1",   "",0,0.2,1,0);
        for(int i = 0;i<2;i++)pads[i]->Draw();
    }
    pads[0]->cd();

    RooPlot* simpleMassFrame    = (RooPlot*)mass->frame();
    
    wdata->plotOn(simpleMassFrame,DataError(RooAbsData::SumW2));
    simpleMassPDF->plotOn(simpleMassFrame,RooFit::Components("massSignalPDF"),LineColor(2));
    simpleMassPDF->plotOn(simpleMassFrame,RooFit::Components("simpleMassBckPDF"),LineColor(1));
    simpleMassPDF->plotOn(simpleMassFrame,RooFit::Components("simpleMass_JpsiX"),LineColor(3));
    simpleMassPDF->plotOn(simpleMassFrame,RooFit::Components("simpleMass_pol"),LineColor(5));
    simpleMassPDF->plotOn(simpleMassFrame,RooFit::Components("simpleMass_exponential1"),LineColor(6));
    simpleMassPDF->plotOn(simpleMassFrame);
    double chi = simpleMassFrame ->chiSquare(nParamsSimple);
    
    simpleMassFrame->SetXTitle("m_{B}");
    simpleMassFrame->SetYTitle("");
    simpleMassFrame->SetTitle(niceName);
    simpleMassFrame->Draw();
     //Nbins [\implemented in chiSquare/] - (Nfreeparams +1 )
    cout<<endl<<chi<< " "<<endl;
    l->AddEntry("",Form("#chi^{2}/NDF = %.2f",chi),"");
    l->Draw();
    
    c1->cd();
    simpleMassFrame->Draw();
    l->Draw();
    
    pads[1]->cd();
    RooHist* hpullSimpleMass = simpleMassFrame->pullHist() ;
    RooPlot* simpleMassFramePull = (RooPlot*)mass->frame(Title(niceName+" Pull Distribution Data")) ;
    simpleMassFramePull->addPlotable(hpullSimpleMass,"P") ;
    simpleMassFramePull->SetXTitle("m_{B}");
    
    TLine * line0 = new TLine(simpleMassFramePull-> GetXaxis()->GetXmin(),  0,  simpleMassFramePull-> GetXaxis()->GetXmax(), 0);
    TLine * linep1 = new TLine(simpleMassFramePull-> GetXaxis()->GetXmin(), 1,  simpleMassFramePull-> GetXaxis()->GetXmax(), 1);
    TLine * linem1 = new TLine(simpleMassFramePull-> GetXaxis()->GetXmin(),-1,  simpleMassFramePull-> GetXaxis()->GetXmax(),-1);
    TLine * linep3 = new TLine(simpleMassFramePull-> GetXaxis()->GetXmin(), 3,  simpleMassFramePull-> GetXaxis()->GetXmax(), 3);
    TLine * linem3 = new TLine(simpleMassFramePull-> GetXaxis()->GetXmin(),-3,  simpleMassFramePull-> GetXaxis()->GetXmax(),-3);
    line0 -> SetLineColor(3);
    linep1 -> SetLineColor(3);
    linem1 -> SetLineColor(3);
    linep3 -> SetLineColor(2);
    linem3 -> SetLineColor(2);
    linep1 -> SetLineStyle(2);
    linem1 -> SetLineStyle(2);
    linep3 -> SetLineStyle(2);
    linem3 -> SetLineStyle(2);
    
    
    
    simpleMassFramePull->Draw();

    
    c2->cd();
    simpleMassFramePull->Draw();
    line0  ->Draw();
    linep1 ->Draw();
    linem1 ->Draw();
    linep3 ->Draw();
    linem3 ->Draw();
    
    c3->cd();
    RooHist* hresSimpleMass = simpleMassFrame->residHist() ;
    RooPlot* simpleMassFrameRes = (RooPlot*)mass->frame(Title(niceName+" Residual Distribution Data")) ;
    simpleMassFrameRes->addPlotable(hresSimpleMass,"P") ;
    simpleMassFrameRes->SetXTitle("m_{B}");
    
    simpleMassFrameRes->Draw();
    line0  ->Draw();
    
    TCanvas *cErr = new TCanvas("cErr","",1200,900);
    TCanvas *cTot = new TCanvas("cTot","",1200,900);
    TCanvas *cErrPull = new TCanvas("cErrPull","",1200,240);
    TCanvas *cTotPull = new TCanvas("cTotPull","",1200,240);
    
    if(useMassPCE){

        pads[4]->cd();
        RooPlot* massErrFrame = (RooPlot*)massErr->frame();
        
        wdata->plotOn(massErrFrame,DataError(RooAbsData::SumW2));
        totPunziPdfMassErr->plotOn(massErrFrame);
        totPunziPdfMassErr->plotOn(massErrFrame,RooFit::Components("signalPunziPDFMassErr"),LineColor(2));
        totPunziPdfMassErr->plotOn(massErrFrame,RooFit::Components("backgroundPunziPDFMassErr"),LineColor(1));
        
//         double chi = totPunziPdfMassErr ->chiSquare(nParamsSimple);
    
        massErrFrame->SetXTitle("#sigma(m_{B})");
        massErrFrame->SetYTitle("");
        massErrFrame->SetTitle(niceName + " Punzi Pdf Mass Error");
        massErrFrame->Draw();
        //Nbins [\implemented in chiSquare/] - (Nfreeparams +1 )
//         cout<<endl<<chi<< " "<<endl;
//         l->AddEntry("",Form("#chi^{2}/NDF = %.2f",chi),"");
//         l->Draw();
//         
        cErr->cd();
        massErrFrame->Draw();
//         l->Draw();

        massErrFrame->Draw();
        pads[5]->cd();
        RooHist* hpullSimpleMassMassErr = massErrFrame->pullHist() ;
        RooPlot* massErrFramePull = (RooPlot*)massErr->frame() ;
        massErrFramePull->addPlotable(hpullSimpleMassMassErr,"P") ;
        
        massErrFramePull->SetXTitle("#sigma(m_{B})");
        massErrFramePull->SetYTitle("");
        massErrFramePull->SetTitle(niceName + " Punzi Pdf Mass Error Pull Distribution");
        massErrFramePull->Draw();

        cErrPull->cd();
        massErrFramePull->Draw();
        
        pads[6]->cd();
        RooPlot* massFrame    = (RooPlot*)mass->frame();

        wdata->plotOn(massFrame,DataError(RooAbsData::SumW2));
        totPDF->plotOn(massFrame,RooFit::Components("massSignalPDFCond"),LineColor(2));
        totPDF->plotOn(massFrame,RooFit::Components("massBackgroundPDFCond"),LineColor(1));
        totPDF->plotOn(massFrame,RooFit::Components("mass_JspiX"),LineColor(3));
        totPDF->plotOn(massFrame);
        
        massFrame->SetXTitle("m_{B}");
        massFrame->SetYTitle("");
        massFrame->SetTitle(niceName);
            
        double chi2Tot = massFrame ->chiSquare(nParamsTot); //Nbins [\implemented in chiSquare/] - (Nfreeparams +1 )
        cout<<endl<<chi2Tot<<" "<<endl;
        l2->AddEntry("",Form("#chi^{2}/NDF = %.2f",chi2Tot),"");
        l2->Draw();
        
        massFrame->Draw();
        
        cTot->cd();
        massFrame->Draw();
        l2->Draw();
        
        pads[7]->cd();
        RooHist* hpullSimpleMassMass = massFrame->pullHist() ;
        RooPlot* massFramePull = (RooPlot*)mass->frame() ;
        massFramePull->addPlotable(hpullSimpleMassMass,"P") ;
        
        
        massFramePull->SetXTitle("#m_{B}");
        massFramePull->SetYTitle("");
        massFramePull->SetTitle(niceName + " Punzi Pdf Mass Error Pull Distribution");
        massFramePull->Draw();
        
        massFramePull->Draw();
        
        cTotPull->cd();
        massFramePull->Draw();
        
        
    }
    result->Print();
    TF1* func = (TF1*) simpleMassPDF->asTF(*mass,*params,*mass);
    
    sgDescription += "_"+bgDescription;
    if (tag != "") sgDescription += "_" + tag;
    if(years.find(year) != years.end())c->SaveAs(Form("../run/BdMass%sAll.pdf",year.Data()));
    else c->SaveAs("../run/BdMassFullRun2_"+sgDescription+"All.pdf");
    c1->SaveAs("../run/BdMassFullRun2_"+sgDescription+".pdf");
    func->SetLineColor(2);
    func->SetLineStyle(10);
    func->Draw("same");
    c1->SaveAs("../run/BdMassFullRun2_"+sgDescription+"_check.pdf");
    c1->SetLogy();  
    gPad->Modified();
    gPad->Update();
    c1->Draw();
    c1->SaveAs("../run/BdMassFullRun2_"+sgDescription+"_log.pdf");
    c2->SaveAs("../run/BdMassFullRun2_"+sgDescription+"_pull.pdf");
    c3->SaveAs("../run/BdMassFullRun2_"+sgDescription+"_res.pdf");
    
    if(useMassPCE){
        cErr->SaveAs("../run/BdMassFullRun2_"+sgDescription+"Err.pdf");
        cErrPull->SaveAs("../run/BdMassFullRun2_"+sgDescription+"ErrPull.pdf");
        cTot->SaveAs("../run/BdMassFullRun2_"+sgDescription+    "Tot.pdf");
        cTotPull->SaveAs("../run/BdMassFullRun2_"+sgDescription+"TotPull.pdf");
        cTot->SetLogy();  
        gPad->Modified();
        gPad->Update();
        cTot->Draw();
        cTot->SaveAs("../run/BdMassFullRun2_"+sgDescription+    "TotLog.pdf");
        
        cTotPull->SaveAs("../run/BdMassFullRun2_"+sgDescription+"TotPull.pdf");
    }    
    Double_t maximum = func->GetMaximumX();
    std::ofstream outfile;
    //const TMatrixDSym &cor = result->correlationMatrix();
    const TMatrixDSym &cov = result->covarianceMatrix();
    Double_t x,y;
    Int_t counterp=0;
    Int_t countern=0;
    Double_t maxcheck=0,mincheck=0;
//     Double_t meanCheck=0;
    for (int i=0; i<hpullSimpleMass->GetN();++i){
    
        hpullSimpleMass->GetPoint(i,x,y);
//         cout<<x<<" "<<y<<endl;
        if (y>3) counterp++;
        else if (y<-3) countern++;
        if (y>maxcheck)      maxcheck=y;
        else if (y<mincheck) mincheck=y;
        
//         meanCheck+=y;
    }
    params->writeToFile("../run/BdMassFullRun2_"+sgDescription+"_params.out");
    outfile.open("../run/BdMassFullRun2_"+sgDescription+".txt", std::ios_base::out); // append instead of overwrite
    outfile << "chi2/ndf: "<<Form("%.3f",chi)<<endl;//", pullsOff+-3: "<<counterp+countern<<"; (>+3)"<<counterp<<" (<-3)"<<countern<<endl;
    outfile << "pullsOff+-3: "<<counterp+countern<<" largerThan+3: "<<counterp<<"smallerThan-3: "<<countern<<endl;
    outfile <<"pullMin: "<< Form("%.2f",mincheck)<<", pullMax: "<<Form("%.2f",maxcheck)<<endl; 
    outfile <<"pullMean: "<< Form("%.3f",hpullSimpleMass->GetMean(2))<<", pullRMS: "<<Form("%.3f",hpullSimpleMass->GetRMS(2))<<endl; 
    outfile <<"fittedBmass: "<< Form("%.2f",maximum)<<endl; 
    outfile <<"-NLL: "<<result->minNll()<<endl;
    result->printMultiline(outfile,100,kTRUE);
    outfile.close();
    
    setGoodMiddleScaleSymmetric();
    gStyle->SetPaintTextFormat("1.3f");
    TCanvas *cCor = new TCanvas("cCor","",1200,900);
//     for(int i=0; i<simpleMassFramePull->GetNbins();++i) cout<<simpleMassFramePull->GetBinContent(i)<<endl;
    TH2 *cor = result->correlationHist();
    cCor->cd();
    cor->Draw("text colz");
    gStyle->SetOptStat(0);
    cCor->SaveAs("../run/BdMassFullRun2_"+sgDescription+"_cor.pdf");	
    
}
