#include "RooAbsPdf.h"
#include "RooAbsReal.h"
#include "RooRealProxy.h"


#ifndef RooTanhPdf_h
	#define RooTanhPdf_h


	class RooTanhPdf : public RooAbsPdf
	{
		public:
			RooTanhPdf()
			{
			};

			RooTanhPdf(const char *name, const char *title, RooAbsReal &_x, RooAbsReal &_s, RooAbsReal &_o);
			RooTanhPdf(const RooTanhPdf &other, const char *name = 0);

			virtual TObject *clone(const char *newname) const
			{
				return new RooTanhPdf(*this, newname);
			};

			inline virtual ~RooTanhPdf()
			{
			};


			virtual Int_t getAnalyticalIntegral(RooArgSet &allVars, RooArgSet &analVars, const char *rangeName = 0) const;
			virtual Double_t analyticalIntegral(Int_t code, const char *rangeName = 0) const;


		protected:
			RooRealProxy x;
			RooRealProxy s;
			RooRealProxy o;


			Double_t evaluate() const;


		private:
#ifdef __CINT__ /* HACK */
			ClassDef(RooTanhPdf, 1);
#endif /* __CINT__ HACK */
	};

#endif
