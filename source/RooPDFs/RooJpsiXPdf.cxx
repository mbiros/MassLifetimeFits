#include "RooJpsiXPdf.h" 


#include "TIterator.h"
#include "TMath.h"

#ifdef __CINT__ /* HACK */
ClassImp(RooJpsiXPdf)
#endif /* __CINT__ HACK */


RooJpsiXPdf::RooJpsiXPdf(const char *name, const char *title, RooAbsReal &_x, RooAbsReal &_m0, RooAbsReal &_s) : RooAbsPdf(name, title), x("x", "x", this, _x), m0("m0", "m0", this, _m0), s("s", "s", this, _s)
{
};

RooJpsiXPdf::RooJpsiXPdf(const RooJpsiXPdf &other, const char *name) : RooAbsPdf(other, name), x("x", this, other.x), m0("m0", this, other.m0), s("s", this, other.s)
{
};


Double_t RooJpsiXPdf::evaluate() const
{
	return(1-(s*(x-m0))/TMath::Sqrt(1+TMath::Power(s*(x-m0),2) ));
};


Int_t RooJpsiXPdf::getAnalyticalIntegral(RooArgSet &allVars, RooArgSet &analVars, const char *rangeName) const
{
	if(true == matchArgs(allVars, analVars, x))
	{
		return(1);
	}
	else
	{
		return(0);
	};
};

Double_t RooJpsiXPdf::analyticalIntegral(Int_t code, const char *rangeName) const
{
	const Double_t xMax = x.max(rangeName);
	const Double_t xMin = x.min(rangeName);

	const Double_t UpperLimit = xMax-(TMath::Sqrt(TMath::Power(s*(xMax-m0),2)+1))/(s);
	const Double_t LowerLimit = xMin-(TMath::Sqrt(TMath::Power(s*(xMin-m0),2)+1))/(s);

	const Double_t Integral = UpperLimit - LowerLimit;
	return(Integral);
};
