#include "RooAbsPdf.h"
#include "RooAbsReal.h"
#include "RooRealProxy.h"


#ifndef RooErfPdf_h
	#define RooErfPdf_h


	class RooErfPdf : public RooAbsPdf
	{
		public:
			RooErfPdf()
			{
			};

			RooErfPdf(const char *name, const char *title, RooAbsReal &_x, RooAbsReal &_m0, RooAbsReal &_s);
			RooErfPdf(const RooErfPdf &other, const char *name = 0);

			virtual TObject *clone(const char *newname) const
			{
				return new RooErfPdf(*this, newname);
			};

			inline virtual ~RooErfPdf()
			{
			};


			virtual Int_t getAnalyticalIntegral(RooArgSet &allVars, RooArgSet &analVars, const char *rangeName = 0) const;
			virtual Double_t analyticalIntegral(Int_t code, const char *rangeName = 0) const;


		protected:
			RooRealProxy x;
			RooRealProxy m0;
			RooRealProxy s;


			Double_t evaluate() const;


		private:
#ifdef __CINT__ /* HACK */
			ClassDef(RooErfPdf, 1);
#endif /* __CINT__ HACK */
	};

#endif
