// Author: Jonas Rembser, CERN  02/2021
  
 #ifndef RooFit_RooFit_RooCrystalBall_h
 #define RooFit_RooFit_RooCrystalBall_h
  
 #include "RooAbsPdf.h"
 #include "RooRealProxy.h"
  
 #include <memory>
  
 class RooRealVar;
  
 class RooCrystalBall final : public RooAbsPdf {
 public:
  
    RooCrystalBall(){};
    
//     //added s1 s2
//     RooCrystalBall(const char *name, const char *title, RooAbsReal &x, RooAbsReal &x0, RooAbsReal &sigma,
//                    RooAbsReal &alphaL, RooAbsReal &nL, RooAbsReal &alphaR, RooAbsReal &nR, RooAbsReal& sL1, RooAbsReal& sL2, RooAbsReal& sR1, RooAbsReal& sR2);
    
    //added s1 s2 s0
    RooCrystalBall(const char *name, const char *title, RooAbsReal &x, RooAbsReal &x0, RooAbsReal &sigma,
                   RooAbsReal &alphaL, RooAbsReal &nL, RooAbsReal &alphaR, RooAbsReal &nR, RooAbsReal& sL1, RooAbsReal& sL2, RooAbsReal& sR1, RooAbsReal& sR2, RooAbsReal& sL0, RooAbsReal& sR0);
  
    RooCrystalBall(const RooCrystalBall &other, const char *name = 0);
    virtual TObject *clone(const char *newname) const { return new RooCrystalBall(*this, newname); }
  
    inline virtual ~RooCrystalBall() {}
  
    virtual Int_t getAnalyticalIntegral(RooArgSet &allVars, RooArgSet &analVars, const char *rangeName = 0) const;
    virtual Double_t analyticalIntegral(Int_t code, const char *rangeName = 0) const;
  
    // Optimized accept/reject generator support
    virtual Int_t getMaxVal(const RooArgSet &vars) const;
    virtual Double_t maxVal(Int_t code) const;
  
 protected:
    Double_t evaluate() const;
  
 private:
    RooRealProxy x_;
    RooRealProxy x0_;
    RooRealProxy sigmaL_;
    RooRealProxy sigmaR_;
    RooRealProxy alphaL_;
    RooRealProxy nL_;
    
    // optional parameters
    RooRealProxy alphaR_ ;
    RooRealProxy nR_     ;
  
    RooRealProxy sL1_; //efficient sigma
    RooRealProxy sL2_;
  
    RooRealProxy sR1_; //efficient sigma
    RooRealProxy sR2_;
    
    RooRealProxy sL0_; //efficient sigma
    RooRealProxy sR0_;
    
    ClassDef(RooCrystalBall, 1)
 };
  
 #endif
