#include "RooErfPdf.h" 


#include "TIterator.h"
#include "TMath.h"

#ifdef __CINT__ /* HACK */
ClassImp(RooErfPdf)
#endif /* __CINT__ HACK */


RooErfPdf::RooErfPdf(const char *name, const char *title, RooAbsReal &_x, RooAbsReal &_m0, RooAbsReal &_s) : RooAbsPdf(name, title), x("x", "x", this, _x), m0("m0", "m0", this, _m0), s("s", "s", this, _s)
{
};

RooErfPdf::RooErfPdf(const RooErfPdf &other, const char *name) : RooAbsPdf(other, name), x("x", this, other.x), m0("m0", this, other.m0), s("s", this, other.s)
{
};


Double_t RooErfPdf::evaluate() const
{
	return(1-TMath::Erf((x-m0)/s));
};


Int_t RooErfPdf::getAnalyticalIntegral(RooArgSet &allVars, RooArgSet &analVars, const char *rangeName) const
{
	if(true == matchArgs(allVars, analVars, x))
	{
		return(1);
	}
	else
	{
		return(0);
	};
};

Double_t RooErfPdf::analyticalIntegral(Int_t code, const char *rangeName) const
{
	const Double_t xMax = x.max(rangeName);
	const Double_t xMin = x.min(rangeName);

	const Double_t xMaxArg = (xMax-m0)/s;
	const Double_t xMinArg = (xMin-m0)/s;


	const Double_t UpperLimit = xMax-s*(xMaxArg*TMath::Erf(xMaxArg)+TMath::Exp(-xMaxArg*xMaxArg)/TMath::Sqrt(TMath::Pi()));
	const Double_t LowerLimit = xMin-s*(xMinArg*TMath::Erf(xMinArg)+TMath::Exp(-xMinArg*xMinArg)/TMath::Sqrt(TMath::Pi()));

	const Double_t Integral = UpperLimit - LowerLimit;
	return(Integral);
};
